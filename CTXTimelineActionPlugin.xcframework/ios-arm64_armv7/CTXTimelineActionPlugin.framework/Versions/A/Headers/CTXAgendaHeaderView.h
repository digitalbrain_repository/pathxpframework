//
//  CTXAgendaHeaderView.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <UIKit/UIKit.h>

#import <CTXTimelineActionPlugin/CTXTimelineActionStyle.h>

@interface CTXAgendaHeaderView : UIView <CTXTimelineActionStylable>

@property (nonatomic, weak) IBOutlet UILabel *titleLbl;

@property (nonatomic) IBOutletCollection(UILabel) NSMutableArray * stylableDate;
@property (nonatomic) IBOutletCollection(UIView) NSMutableArray * stylableFrame;

@end
