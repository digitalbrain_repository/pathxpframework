//
//  CTXAgendaViewController.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <UIKit/UIKit.h>
#import <CTXTimelineActionPlugin/CTXTimelineActionInfoResponse.h>
#import <CTXSDK/CTXBaseActionViewController.h>
#import <CTXTimelineActionPlugin/CTXTimelineActionStyle.h>

@interface CTXAgendaViewController : CTXBaseActionViewController <UITableViewDataSource,UITableViewDelegate, CTXTimelineActionStylable>
@property (nonatomic,weak) IBOutlet UITableView *tableView;
@property (nonatomic,weak) IBOutlet UIView *tableViewHeader;
@property (nonatomic,strong) CTXTimelineActionInfoResponseBody *timelineInfo;
@property (nonatomic) NSUInteger currentEventIndex;

@property (nonatomic) IBOutletCollection(UIImageView) NSMutableArray * stylableHeader;

@end
