//
//  CTXAgendaCell.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <UIKit/UIKit.h>

#import <CTXTimelineActionPlugin/CTXTimelineActionStyle.h>

@interface CTXAgendaCell : UITableViewCell <CTXTimelineActionStylable>
@property (nonatomic, weak) IBOutlet UILabel *titleLbl;
@property (nonatomic, weak) IBOutlet UILabel *timeLbl;
@property (nonatomic, weak) IBOutlet UILabel *descrLbl;
@property (nonatomic, weak) IBOutlet UIView  *containerView;

@property (nonatomic) IBOutletCollection(UIImageView) NSMutableArray * stylableClockIco;
@property (nonatomic) IBOutletCollection(UILabel) NSMutableArray * stylableCardTitle;
@property (nonatomic) IBOutletCollection(UILabel) NSMutableArray * stylableCardDescription;
@property (nonatomic) IBOutletCollection(UILabel) NSMutableArray * stylableTime;
@property (nonatomic) IBOutletCollection(UIView) NSMutableArray * stylableFrame;




@property (nonatomic, strong) UIView *separator;
- (void)setDescrLblText:(NSString *)text;
- (CGFloat)cellOpenedHeigth;
- (void)setAsLastCell:(BOOL)isLast;
@end
