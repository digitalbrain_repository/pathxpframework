//
//  CTXTimelineScrollViewDelegate.h
//  TimelineActionPlugin
//
//  Created by Massimiliano Schinco on 01/08/16.
//  Copyright © 2016 com.mxm345. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CTXTimelineActionPlugin/CTXCollectionViewIndicatorView.h>

@interface CTXTimelineScrollViewDelegate : NSObject <UIScrollViewDelegate>
@property (nonatomic, weak) UICollectionView *collectionView;
@property (nonatomic, weak) CTXCollectionViewIndicatorView *indicatorScrollView;
@property (nonatomic, weak) CTXScrollViewCustomPage *fakeScrollView;

@end
