//
//  CTXSectionActionPlugin.h
//  DefaultPlugins
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <CTXSDK/CTXActionPlugin.h>
#import <CTXSectionActionPlugin/CTXSectionActionInfoResponse.h>

@class CTXSectionActionPlugin;

@protocol CTXSectionActionDelegate <NSObject>

- (void) sectionActionPlugin: (CTXSectionActionPlugin*) plugin showSectionWithInfo: (CTXSectionActionInfoResponseBody*) info;

@end

@interface CTXSectionActionPlugin : CTXActionPlugin

@property (nonatomic, weak) id<CTXSectionActionDelegate> sectionActionDelegate;

@end
