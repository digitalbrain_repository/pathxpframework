//
//  CTXPOIAnnotation.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import <CTXPOIActionPlugin/CTXPOI.h>

@interface CTXPOIAnnotation : NSObject  <MKAnnotation>

- (instancetype)initWithPOI:(CTXPOI*)poi;

@property (nonatomic, readonly) CTXPOI *selectedPOI;

@end
