//
//  CTXPOIActionStyle.h
//  POIActionPlugin
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <CTXSDK/CTXBaseActionStyle.h>

@protocol CTXPOIActionStylable <CTXBaseActionStylable>

@property (nonatomic) IBOutletCollection(UIView) NSMutableArray* stylableFrame;
@property (nonatomic) IBOutletCollection(UIButton) NSMutableArray* stylableTabMapButton;
@property (nonatomic) IBOutletCollection(UIButton) NSMutableArray* stylableTabListButton;

@property (nonatomic) IBOutletCollection(UILabel) NSMutableArray* stylableListText;
@property (nonatomic) IBOutletCollection(UIImageView) NSMutableArray* stylableListArrow;
@property (nonatomic) IBOutletCollection(UIView) NSMutableArray* stylableListSeparator;

@property (nonatomic) IBOutletCollection(UIView) NSMutableArray* stylableListShadow;
@property (nonatomic) IBOutletCollection(UIImageView) NSMutableArray* stylableListShadowMarker;
@property (nonatomic) IBOutletCollection(UILabel) NSMutableArray* stylableListShadowText;

@property (nonatomic) IBOutletCollection(UIImageView) NSMutableArray* stylableVideoLinkIcon;
@property (nonatomic) IBOutletCollection(UILabel) NSMutableArray* stylableVideoLinkText;

@property (nonatomic) IBOutletCollection(UIView) NSMutableArray* stylableMapPoiFrame;
@property (nonatomic) IBOutletCollection(UIButton) NSMutableArray* stylableMapPoiFrameButton;
@property (nonatomic) IBOutletCollection(UILabel) NSMutableArray* stylableMapPoiFrameTitle;
@property (nonatomic) IBOutletCollection(UILabel) NSMutableArray* stylableMapPoiFrameAddress;




@end

@interface CTXPOIActionStyle : CTXBaseActionStyle

@property (nonatomic) UIColor * frameBgColor;
@property (nonatomic) CTXImageResource * tabListSelectedIco;
@property (nonatomic) CTXImageResource * tabListUnselectedIco;
@property (nonatomic) CTXImageResource * tabMapSelectedIco;
@property (nonatomic) CTXImageResource * tabMapUnselectedIco;

@property (nonatomic) CTXFontResource * listFont;
@property (nonatomic) UIColor * listTextColor;
@property (nonatomic) CTXImageResource * listArrow;

@property (nonatomic) UIColor * listShadowColor;
@property (nonatomic) CTXImageResource * listShadowMarker;
@property (nonatomic) CTXFontResource * listShadowFont;
@property (nonatomic) UIColor * listShadowTextColor;

@property (nonatomic) CTXImageResource * videoIco;
@property (nonatomic) CTXFontResource * videoLinkFont;
@property (nonatomic) UIColor * videoLinkTextColor;

@property (nonatomic) CTXImageResource * mapMainMarker;
@property (nonatomic) CTXImageResource * mapMarker;

@property (nonatomic) UIColor * mapPoiFrameBgColor;

@property (nonatomic) UIColor * mapPoiFrameBtBgColor;
@property (nonatomic) CTXFontResource * mapPoiFrameBtFont;
@property (nonatomic) UIColor * mapPoiFrameBtTextColor;

@property (nonatomic) CTXFontResource * mapPoiFrameTitleFont;
@property (nonatomic) UIColor * mapPoiFrameTitleTextColor;

@property (nonatomic) CTXFontResource * mapPoiFrameAddressFont;
@property (nonatomic) UIColor * mapPoiFrameAddressTextColor;

-(void)applyTo:(id<CTXPOIActionStylable>)stylable;

@end
