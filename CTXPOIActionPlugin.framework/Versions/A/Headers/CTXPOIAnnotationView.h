//
//  CTXPOIAnnotationView.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <MapKit/MapKit.h>
#import <CTXPOIActionPlugin/CTXPOICalloutViewController.h>
#import <CTXPOIActionPlugin/CTXPOIActionInfoResponse.h>
#import <CTXPOIActionPlugin/CTXPOIActionStyle.h>

#define CTX_POI_ANNOTATION_VIEW_IDENTIFIER  @"CTXPoiAnnotationViewIdentifier"
static CGFloat const kCTXAnnotationMainPinDim = 40.0;
static CGFloat const kCTXAnnotationSecondaryPinDim = 36.0;

@interface CTXPOIAnnotationView : MKAnnotationView

@property (nonatomic, readonly) CTXPOICalloutViewController *calloutController;
@property (nonatomic) CTXPOIActionInfoResponseBody *poiInfo;
@property (nonatomic) CTXPOI *selectedPOI;
@property (nonatomic, getter=isMain) BOOL main;
@property (nonatomic, weak) CTXPOIActionStyle * style;


-(id)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier storyboard: (UIStoryboard*) storyboard;

@end
