//
//  CTXInteractionManager.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#import <CTXSDK/CTXBeacon.h>
#import <CTXSDK/CTXDashboardEntity.h>
#import <CTXSDK/CTXServiceManager.h>
#import <CTXSDK/CTXTrigger.h>
#import <CTXSDK/CTXGeoRegion.h>
#import <CTXSDK/CTXInteraction.h>
#import <CTXSDK/CTXInteractionState.h>

#pragma mark - Defines

#define CTX_INTERACTION_WORKFLOW    @"actionWorkflow"

@class CTXChannelEntry, CTXInteractionLink, CTXPastEvent;
@class CTXBaseActionViewController;
@class CTXDashboardTileBaseViewController;
@class CTXInteractionTriggersResponse;

typedef void (^CTXInteractionTriggersSuccessResponseHandler)(CTXInteractionTriggersResponse *interactionTriggersResponse);
typedef void (^CTXInteractionTriggersFailureResponseHandler)(NSError *failureError);

typedef void (^CTXInteractionSuccessResponseHandler)(CTXInteractionInfoResponseBody *interactionResponse);
typedef void (^CTXInteractionFailureResponseHandler)(NSError *failureError);

#pragma mark - Protocol
@protocol CTXInteractionDelegate <NSObject>


- (void)handleDashboardAction:(CTXDashboardEntity*)dashboardActionInfo withInteraction:(CTXInteraction*)interaction error:(NSString*)errorMessage completionHandler: (void(^)(void)) completionHandler;


// the new ones:
-(void)createActionFullViewController:(C345Action *)action withInfo:(CTXBaseActionInfoResponseBody *)actionInfo interaction: (CTXInteraction*) interaction completionHandler: (void(^)(CTXBaseActionViewController*)) handler;
- (void)createActionTileViewControllerWithBlock: (CTXDashboardBlock*)block withInfo: (CTXBaseActionInfoResponseBody*)actionInfo interaction: (CTXInteraction*) interaction completionHandler: (void(^)(CTXDashboardTileBaseViewController*)) handler;

-(void)handleActionFull:(C345Action *)action withInfo:(CTXBaseActionInfoResponseBody *)actionInfo interaction: (CTXInteraction*) interaction;

- (void)createDashboardViewController:(CTXDashboardEntity *)dashboardActionInfo withInteraction:(CTXInteraction*)interaction error:(NSString *)errorMessage completionHandler:(void (^)(UIViewController*))handler;

- (void)interactionWillStart:(CTXInteraction*)interaction;
- (void)interactionDidStart:(CTXInteraction*)interaction;

- (int) numberOfChannels;
- (int) depthForChannel: (int) channel;

- (void) setChannelEntries: (NSArray*) entries forcedEntries: (NSArray*) forcedEntries forStoryNavigatorOnState: (CTXInteractionState*) state withCompletionHandler: (void(^)(void)) handler;
- (void) setChannelEntriesForNotificationManager: (NSArray*) entries completionHandler: (void(^)(void)) handler;
- (void) navigateStoryToEntry: (CTXChannelEntry*) entry completionHandler: (void(^)(void)) handler;

- (int) depthForLinks;

@end

@protocol CTXTriggersDataSourceDelegate <NSObject>

@optional
- (void)getTriggersWithCoordinate:(CLLocation *)coordinate
                          success:(CTXInteractionTriggersSuccessResponseHandler)success
                          failure:(CTXInteractionTriggersFailureResponseHandler)failure;

- (void)getInteractionWithId:(NSNumber*)interactionId
                     success:(CTXInteractionSuccessResponseHandler)success
                     failure:(CTXInteractionFailureResponseHandler)failure;

@end

@interface CTXInteractionManager : NSObject

#pragma mark - Shared Instance
+ (instancetype)manager;
+ (instancetype)sharedManager;

#pragma mark - Delegate
@property (nonatomic) id<CTXInteractionDelegate> delegate;
@property (nonatomic) id<CTXTriggersDataSourceDelegate> applicationDelegate;
@property (nonatomic, getter=isInteractionsEnabled) BOOL interactionsEnabled;

#pragma mark - Functions

- (void)getTriggersWithCoordinate:(CLLocation*)coordinate
                          success:(CTXInteractionTriggersSuccessResponseHandler)success
                          failure:(CTXFailedResponseHandler)failure;

- (void)getTriggersWithCoordinate:(CLLocation *)coordinate
                   forceFromCache:(BOOL) forceFromCache
                          success:(CTXInteractionTriggersSuccessResponseHandler)success
                          failure:(CTXFailedResponseHandler)failure;

- (void)startValidInteraction;
- (void)startInteractionWithId:(NSNumber *)interactionId;
- (void)startInteractionWithTrigger: (CTXTrigger*) selectedTrigger;
- (void)startInteractionWithTrigger: (CTXTrigger*)selectedTrigger force: (BOOL) forced;
- (void)removeCurrentInteractions;
- (void)removeCurrentInteractionOnChannel: (int) channel forStates: (NSSet*) states;

- (void) removeEntry: (CTXChannelEntry*) killMe;
- (void) clearForcedChannel: (int) channel forStates: (NSSet*) states;


- (void) getPastEvents: (void(^)(NSArray*)) handler withTriggerTypes: (NSArray*) types maxCount: (int) maxCount withFullIdratation: (BOOL) full forceTriggersUpdate: (BOOL) forceTriggersUpdate;

- (void) pastEventWithActionInfo: (CTXBaseActionInfoResponseBody*) actionInfo completionHandler: (void(^)(CTXPastEvent* event)) handler;

- (void)getActionViewControllerWithChannelEntry:(CTXChannelEntry*)entry
                              completionHandler:(void(^)(UIViewController*))completionHandler;

- (void) performLink: (CTXInteractionLink*) link completionHandler: (void(^)(BOOL, C345Response *)) handler;

- (void) populateEvent: (CTXPastEvent*) event withCompletionHandler: (void(^)(void)) handler;

- (void) addState: (CTXInteractionState*) state;
- (void) removeState: (CTXInteractionState*) state;
- (void) resetStates: (CTXInteractionState*) state;

- (void) addMaskedState: (CTXInteractionState*) state;
- (void) removeMaskedState: (CTXInteractionState*) state;
- (void) resetMaskedStates: (CTXInteractionState*) state;

- (void) resumePersistedForcedInteractionId;

- (void) startFirstValidNoTrigger;

- (void)linkInteraction:(CTXInteractionLink *)interactionLink fromChannelEntry: (CTXChannelEntry*) entry completionHandler: (void(^)(BOOL,C345Response*)) handler;
- (CTXChannelEntry *) resolveLinkEntryWithInteractionId: (NSNumber*) interactionId actionId: (NSNumber*) actionId onState: (CTXInteractionState*) forceState;
@property (nonatomic) CTXInteraction *currentInteraction;

@end
