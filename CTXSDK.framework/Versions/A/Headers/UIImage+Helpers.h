//
//  UIImage+Helpers.h
//  CoreLibrary
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <UIKit/UIKit.h>

@interface UIImage (Helpers)

+ (void)loadFromURL:(NSURL*)url callback:(void(^)(UIImage *image))callback;
+ (void) loadDataFromURL: (NSURL*) url callback:(void (^)(NSData * rawData))callback;

@end
