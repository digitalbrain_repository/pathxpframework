//
//  CTXGetSettingsResponse.h
//  PathXP
//
//  Created by Massimiliano on 03/07/17.
//  Copyright © 2017 01tribe. All rights reserved.
//

#import <CTXSDK/C345Response.h>
@interface CTXSelectOption: CLJastor
@property NSString *label;
@property NSString *value;
@end

@interface CTXSettingsOption: CLJastor
@property (nonatomic) NSNumber *identifier;//
@property (nonatomic) NSString *type;
@property (nonatomic) NSString *label;
@property (nonatomic) NSString *descr;//
@property (nonatomic) NSNumber *editable;
@property (nonatomic) NSNumber *enabled;
@property (nonatomic) NSNumber *forAuthenticathedUser;
@property (nonatomic) NSString *selected;
@property (nonatomic) NSArray <CTXSelectOption *> *selectOptions;
@end

@interface CTXGetSettingsResponseBody : CTXResponseBody
@property (nonatomic) NSArray<CTXSettingsOption*> *options;
@end

@interface CTXGetSettingsResponse : C345Response
@property (nonatomic) CTXGetSettingsResponseBody *body;
@end
