//
//  CTXChannelEntry.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>
#import <CTXSDK/CTXPastEvent.h>

@class CTXInteractionLink, CTXInteractionState;

@interface CTXChannelEntry : CTXPastEvent

@property (nonatomic) NSDate * arrival;
@property (nonatomic) NSNumber * channel;
@property (nonatomic, assign) BOOL forced;
@property (nonatomic) CTXInteractionState * state;

@property (nonatomic) CTXChannelEntry * link;
@property (nonatomic, weak) CTXChannelEntry * backLink;

@property (nonatomic, copy) CTXInteractionLink * generatedBy;

- (NSNumber*) debounceKey;
- (BOOL) isEqualToChannelEntry: (CTXChannelEntry*) other;

- (void) linkTo: (CTXChannelEntry*) other;
- (CTXChannelEntry*) pickLink;
- (void) popLink;
- (BOOL) isInLinkStackOrEqualToSelf: (CTXChannelEntry*) other;

- (void) iterateStack: (void(^)(CTXChannelEntry * e, BOOL * stop)) iterator;

- (void)popLinkWithInteractionID: (NSNumber *) interactionID;
@end
