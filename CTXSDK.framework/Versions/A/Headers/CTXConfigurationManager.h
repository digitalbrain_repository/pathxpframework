//
//  CTXConfigurationManager.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <CTXSDK/CTXConfigurationResponse.h>
#import <CTXSDK/C345Response.h>
#import <CTXSDK/CTXServiceManager.h>

#pragma mark - Defines
typedef NS_ENUM(NSInteger, CTXConfigurationState)
{
    CTXConfigurationStateNotExecuted = 0,
    CTXConfigurationStateInProgress,
    CTXConfigurationStateExecuted,
    CTXConfigurationStateBlocked
};

#define CTX_CONFIGURATION_WORKFLOW      @"configurationManagementWorkflow"

typedef void (^ContextConfigSuccessResponseHandler)(CTXConfigurationResponse *configNonceResponse);

@interface CTXConfigurationManager : NSObject <CTXServiceDelegate>

#pragma mark - SharedInstance
+ (instancetype)manager;
+ (instancetype)sharedManager;

@property (nonatomic) NSString *additionalInfo1;
@property (nonatomic) NSString *additionalInfo2;
@property (nonatomic) NSString *additionalInfo3;

#pragma mark - Functions
- (void)verifyConfigurationWithSuccess:(ContextConfigSuccessResponseHandler)success
                               failure:(CTXFailedResponseHandler)failure;

@end
