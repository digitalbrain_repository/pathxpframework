//
//  CTXUserManager.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>
#import <CTXSDK/CTXUser.h>

@interface CTXUserManager : NSObject

#pragma mark - SharedInstance
+ (instancetype)manager;

@property (nonatomic, readonly) CTXUser *currentUser;

@end
