//
//  CTXActionInfoRequest.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <CTXSDK/C345Request.h>
#import <CTXSDK/CTXInteractionManager.h>

@interface CTXActionInfoRequestBody : CTXRequestBody

#pragma mark - Properties
@property (nonatomic) NSNumber *interactionId;
@property (nonatomic) NSNumber *actionId;
@property (nonatomic) NSNumber *beaconTriggerId;
@property (nonatomic) NSString *beaconMacAddress;
@property (nonatomic) NSString *deferredAt;

@end

@interface CTXActionInfoRequest : C345Request <CTXRequestDelegate>

#pragma mark - Inizializer
- (instancetype)initWithTraceID:(NSString*)traceID actionType:(NSString*)actionType;

@property (nonatomic) NSString* actionType;

@end
