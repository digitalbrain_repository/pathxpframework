//
//  CTXKeychainCache.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>

@interface CTXKeychainCache : NSObject

+ (instancetype) sharedInstance;

+ (void)updateAccessGroup:(NSString *)accessGroup;
+ (NSString *)stringForKey:(NSString *)key;
+ (BOOL)setString:(NSString *)value forKey:(NSString *)key;
+ (BOOL)removeItemForKey:(NSString *)key;
+ (void)flushCache;

- (void)updateAccessGroup:(NSString *)accessGroup;
- (NSString *)stringForKey:(NSString *)key;
- (BOOL)setString:(NSString *)value forKey:(NSString *)key;
- (BOOL)removeItemForKey:(NSString *)key;
- (void)flushCache;



@end
