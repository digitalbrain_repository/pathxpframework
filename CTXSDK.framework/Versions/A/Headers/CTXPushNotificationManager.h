#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CTXSDK/C345Response.h>
#import <CTXSDK/CTXServiceManager.h>
#import <CTXSDK/CTXBaseTriggerManager.h>
#import <UserNotifications/UserNotifications.h>

@class CTXInteraction;

@protocol CTXPushNotificationDelegate <NSObject>

@optional

- (BOOL)hasCustomNotifications;
- (void)presentLocalNotificationWithTrigger:(CTXTrigger*)trigger;

@end

@interface CTXPushNotificationManager : CTXBaseTriggerManager<UNUserNotificationCenterDelegate>

// *** SHARED INSTANCE ***
+ (instancetype)manager;
+ (instancetype)sharedManager;


// *** PROPERTIES ***
@property (nonatomic) NSString *deviceToken;
@property (nonatomic, readonly) BOOL pushNotificationsEnabled;
@property (nonatomic, readonly) NSString *pushServer;
@property (nonatomic) id<CTXPushNotificationDelegate> delegate;

// *** FUNCTIONS ***
- (void)registerAPNS;
//- (void)registerAPNSWithSettings:(UIUserNotificationSettings *)notificationSettings;

- (void)presentLocalNotificationWithTrigger:(CTXTrigger*)trigger generator:(NSString*) generator;
- (void)presentLocalNotificationWithInteraction:(CTXInteraction *)interaction generator:(NSString *)generator;


- (void)subscribeDeviceWithSuccess:(CTXSuccessResponseHandler)success
                           failure:(CTXFailedResponseHandler)failure;

- (void)unsubscribeDeviceWithSuccess:(CTXSuccessResponseHandler)success
                           failure:(CTXFailedResponseHandler)failure;


- (BOOL)compareDeviceToken:(NSString *)deviceToken;

- (void)didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
- (void)didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken;
- (void)didReceiveRemoteNotification:(NSDictionary*)userInfo;
- (void)didReceiveLocalNotification:(UNNotificationRequest *)notification;

- (BOOL)canHandleRemoteNotification:(NSDictionary*)userInfo;
- (void) resetDebouncer;


@end
