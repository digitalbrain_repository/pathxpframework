//
//  CTXStoryNavigatorController.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <UIKit/UIKit.h>
#import <CTXSDK/CoreLibrary.h>
#import <CTXSDK/CTXInteractionState.h>

#define CTX_STORYNAVIGATOR_VIEWCONTROLLER_IDENTIFIER @"CTXStoryNavigatorVCIdentifier"

@class CTXStoryNavigatorSettings;
@class CTXStoryStackViewController;
@class CTXStoryNavigationBar;
@class CTXChannelEntry;
@class CTXStoryNavigatorController;

@protocol CTXStoryNavigationDelegate <NSObject>

- (void) storyNavigatorDidBuildDashboard: (CTXStoryNavigatorController*) sn;
- (void) storyNavigatorDidSwitchInteraction: (CTXStoryNavigatorController*) sn;

@end

@interface CTXStoryNavigatorController : UIViewController



@property (nonatomic, assign) BOOL navigationBarVisible;

@property (nonatomic) NSArray * channelEntries;
@property (nonatomic) NSArray * forcedEntries;
@property (nonatomic) NSArray * currentEntries;

@property (nonatomic) CTXStoryNavigatorSettings * settings;

@property (nonatomic, readonly) CTXChannelEntry * currentChannelEntry;
@property (nonatomic, readonly) CTXChannelEntry * lastChannelEntry;


// exposed only for subclassing purposes:
@property (nonatomic) CTXStoryStackViewController * stack;
@property (nonatomic) CTXStoryNavigationBar * storyNavigationBar;
@property (nonatomic, weak) NSLayoutConstraint * navigationBarHeightConstraint;
@property (weak, nonatomic) IBOutlet UIButton *fallbackCloseButton;
@property (weak, nonatomic) IBOutlet UIView *stackContainer;
@property (weak, nonatomic) IBOutlet UIView *barContainer;

@property (weak, nonatomic) id<CTXStoryNavigationDelegate> storyNavigationDelegate;


- (void) navigateToRootWithCompletion: (void(^)(void)) handler;
- (void) navigateToEntry: (CTXChannelEntry*) entry completionHandler: (void(^)(void)) handler;
- (void) setChannelEntries:(NSArray *)channelEntries withCompletionHandler: (void(^)(void)) handler;
- (void) setForcedEntries:(NSArray *)channelEntries withCompletionHandler: (void(^)(void)) handler;
- (void) closeDashboardWithCompletion:(VoidBlock)handler;

- (void) invalidateCurrentEntry;
- (void) closeCurrentTopmostActionAnimated: (BOOL) animated withCompletionHandler: (void(^)(void)) handler;

- (void) subscribeToState: (CTXInteractionState*) state;
- (BOOL) isSubscribedToState: (CTXInteractionState*) state;
- (void) autoclose;
- (void) clearAllLinks;
@end
