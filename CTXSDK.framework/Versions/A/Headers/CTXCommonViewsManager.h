
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface CTXCommonViewsManager : NSObject

+ (instancetype)manager;

#pragma mark - Spinner view
- (void)showSpinnerViewOnViewController:(UIViewController *)sender;
- (void)hideSpinnerView;

@end
