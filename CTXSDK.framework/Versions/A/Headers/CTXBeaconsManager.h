//
//  CTXBeaconsManagerPlus.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <CTXSDK/CTXBaseLocatorManager.h>

@interface CTXBeaconsManager : CTXBaseLocatorManager

/**
 *  Singleton Instance
 *
 *  @return Return the singleton instance of CTXBeaconsManager
 */
+ (instancetype)manager;

- (void) simulateBeaconWithMac: (NSString*) mac;
- (void) cleanTriggers;

- (void)didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
- (void) ensureTriggers;
- (void) ensureTriggersWithCompletionHandler: (void(^)(void)) handler;

/*
- (void) startMonitoring;
- (void) stopMonitoring;
*/

@end
