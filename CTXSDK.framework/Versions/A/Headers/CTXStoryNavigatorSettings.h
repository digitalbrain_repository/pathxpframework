//
//  CTXStoryNavigatorSettings.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <UIKit/UIKit.h>

@interface CTXStoryNavigatorSettings : NSObject

@property (nonatomic, getter = isClosable) BOOL closable;
@property (nonatomic) NSNumber * supportedOrientationsMask;
@property (nonatomic, assign) double navigationBarHeight;
@property (nonatomic, assign) BOOL navigationBarHeightIsRelative;
@property (nonatomic, assign) BOOL presentedByApplication;
@property (nonatomic, assign) BOOL forcePriority;
@property (nonatomic, assign) BOOL clearLinksOnNavigation;

@end
