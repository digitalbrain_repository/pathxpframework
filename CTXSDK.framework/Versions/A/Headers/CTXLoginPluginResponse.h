//
//  CTXLoginPluginResponse.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>

@class CTXLoginPluginResponse;

#pragma mark - Defines
typedef void (^CTXLoginPluginSuccessResponseHandler)(CTXLoginPluginResponse *loginResponse);

@interface CTXLoginPluginResponse : NSObject

#pragma mark - Inizializer
- (instancetype)initWithAccessToken:(NSString*)accessToken;

#pragma mark - Properties
@property (nonatomic, readonly) NSString *accessToken;

@end
