//
//  CTXStoryNavigatorEmbeddableController.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <UIKit/UIKit.h>
#import <CTXSDK/CTXChannelEntry.h>

@class CTXStoryNavigatorController;
@class CTXActionPlugin;


@interface CTXStoryNavigatorEmbeddableController : UIViewController


@property (nonatomic, weak) CTXActionPlugin * responsibleActionPlugin;
@property (nonatomic, assign) NSUInteger storyNavigatorPageIndex;
@property (nonatomic) CTXChannelEntry * channelEntry;

@property (nonatomic) NSNumber * containingInteractionId;

- (CTXInteractionLink*) newInteractionLink;

- (CTXStoryNavigatorController*) parentStoryNavigator;

@end
