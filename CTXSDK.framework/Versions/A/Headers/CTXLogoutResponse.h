//
//  CTXLogoutResponse.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <CTXSDK/C345Response.h>

@class CTXLogoutResponse;

#pragma mark - Defines
typedef void (^CTXLogoutSuccessResponseHandler)(CTXLogoutResponse *logoutResponse);

#pragma mark - Class CTXLogoutResponse
@interface CTXLogoutResponse : C345Response

@end
