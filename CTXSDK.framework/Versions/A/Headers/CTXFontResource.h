//
//  CTXFontResource.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <CTXSDK/CTXBundleResource.h>

#define CTX_R_FONT(_name_, _size_, _bundle_) [[CTXFontResource alloc] initWithFontName:(_name_) pointSize:(_size_) bundle:(_bundle_)]

#define CTX_R_APP_FONT(_name_, _size_) CTX_R_FONT(_name_, _size_, [NSBundle mainBundle])

@interface CTXFontResource : CTXBundleResource

@property (readonly) CGFloat pointSize;

- (instancetype) initWithFontName: (NSString*) fontName
                        pointSize: (CGFloat) size
                           bundle: (NSBundle*) bundle;

- (void) applyTo: (UIView*) fontRecipient;

@end
