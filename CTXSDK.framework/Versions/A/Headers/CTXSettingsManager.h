//
//  CTXSettingsManager.h
//  PathXP
//
//  Created by Massimiliano on 04/07/17.
//  Copyright © 2017 01tribe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CTXSDK/CTXGetSettingsResponse.h>

@interface CTXSettingsManager : NSObject

+ (void)getOptionsWithCompletion:(void(^)(CTXGetSettingsResponse* response)) completion;
+ (void)setOptions:(NSArray<CTXSettingsOption *> *)oprtions withCompletion:(void(^)(C345Response* response)) completion;
@end
