//
//  UILabel+UILabel_Animation.h
//  ValentinoFrame
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <UIKit/UIKit.h>

@interface UILabel (ALAnimation)

- (void) setText:(NSString *)text withFadeInDuration: (double) duration;
- (void) setText:(NSString *)text withFadeInDuration: (double) duration delay: (double) delay;


@end
