//
//  CTXCallToAction.h
//  PathXP
//
//  Created by Massimiliano Schinco on 14/06/16.
//  Copyright © 2016 01tribe. All rights reserved.
//

#import <CTXSDK/CLJastor.h>

@interface CTXCallToAction : CLJastor
@property (nonatomic, copy) NSNumber * _Nullable eventId;
@property (nonatomic, copy) NSNumber * _Nullable actionId;
@property (nonatomic, copy) NSString * _Nullable label;
@property (nonatomic, copy) NSString * _Nullable color;

@property (nonatomic, copy) NSString * _Nullable url;
@property (nonatomic, copy) NSString * _Nullable actionType;
@property (nonatomic, copy) NSNumber * _Nullable order;
@property (nonatomic, copy) NSString * _Nullable position;
@property (nonatomic, copy) NSString * _Nullable textColor;
@property (nonatomic, copy) NSString * _Nullable borderColor;
@property (nonatomic, copy) NSString * _Nullable borderWidth;
@property (nonatomic, copy) NSString * _Nullable shadowColor;


@property (nonatomic, copy) NSString * _Nullable shadowRadius;
@property (nonatomic, copy) NSString * _Nullable backgroundColor;
@property (nonatomic, copy) NSString * _Nullable cornerRadius;

@end



/*

 - "position" : "bottom",
 _ "order" : 1,
 "borderWidth" : "2",
 "borderColor" : "#FFFFFF",
 "actionId" : 0,
 "eventId" : 0,
 "label" : "SHOP NOW",
 "textColor" : "#FFFFFF",
 "url" : "https:\/\/www.valentino.com\/it-it\/"
 */
