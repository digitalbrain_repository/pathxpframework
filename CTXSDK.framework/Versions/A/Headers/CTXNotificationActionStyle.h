//
//  CTXNotificationActionStyle.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <CTXSDK/CTXBaseActionStyle.h>


@protocol CTXNotificationActionStylable <CTXBaseActionStylable>

@property (nonatomic) IBOutletCollection(UIView) NSMutableArray * stylableSeparator;

@end

@interface CTXNotificationActionStyle : CTXBaseActionStyle

@property (nonatomic) UIColor * separatorColor;
@property (nonatomic) CTXImageResource * fallbackIcon;

-(void)applyTo:(id<CTXNotificationActionStylable>)stylable;

@end
