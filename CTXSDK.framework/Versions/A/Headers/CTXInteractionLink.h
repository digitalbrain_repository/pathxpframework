//
//  CTXInteractionLink.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>

@class CTXChannelEntry, CTXInteractionState;

@interface CTXInteractionLink : NSObject

@property (nonatomic) CTXInteractionState* state;

@property (nonatomic) NSNumber * sourceInteractionId;
@property (nonatomic) NSNumber * sourceActionId;

@property (nonatomic) NSNumber * targetInteractionId;
@property (nonatomic) NSNumber * targetActionId;

@property (nonatomic, assign) BOOL direct;

@end
