//
//  CTXImageCache.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <UIKit/UIKit.h>

@interface CTXImageCache : NSObject

///@name Singleton pattern

/**
 *  Singleton instance.
 *
 *  @return the singleton instance
 */
+ (instancetype) instance;

///@name Images from cache
/**
 *  Get an image data from the cache given its 345mxm id. If it is not in cache, it will be downloaded on demand and stored in the cache.
 *
 *  @param imageId the image id (as NSNumber)
 */

- (void) imageDataWithId: (NSNumber *) imageId completionHandler: (void(^)(NSData* imageData)) handler;

/**
 *  Get an image from the cache given its 345mxm id. If it is not in cache, it will be downloaded on demand and stored in the cache.
 *
 *  @param imageId the image id (as NSNumber)
 */
- (void) imageWithId: (NSNumber*) imageId completionHandler: (void(^)(UIImage* image)) handler;

/**
 *  Get an image from the cache given its URL. If it is not in cache, it will be downloaded on demand and stored in the cache.
 *
 *  @param imageURL the image URL
 */
- (void) imageWithURL: (NSURL*) imageURL completionHandler: (void(^)(UIImage* image)) handler;


///@name Update cache

/**
 *  Download an image from the given URL, regardless of its presence in cache. On success, the cache gets updated with the new image.
 *
 *  @param imageURL the image URL
 */
- (void) downloadImageWithURL: (NSURL*) imageURL completionHandler: (void(^)(NSData* image)) handler;
- (void) downloadImageWithId: (NSNumber*) imageId completionHandler: (void(^)(UIImage* image)) handler;

@end
