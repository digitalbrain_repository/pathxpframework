//
//  CTXVolantinoManager.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>
#import <CTXSDK/CTXVoltantinoResponse.h>
#import <CTXSDK/CTXServiceManager.h>

#pragma mark - Defines

typedef void (^CTXVolantinoSuccessResponseHandler)(CTXVoltantinoResponse *volantinoResponse);

@interface CTXVolantinoManager : NSObject

#pragma mark - Shared Instance
+ (instancetype)manager;

#pragma mark - Functions
- (void)getVolantinoWithWorkflowName:(NSString*)workflowName
                             success:(CTXVolantinoSuccessResponseHandler)success
                             failure:(CTXFailedResponseHandler)failure;


- (UIImage*)getVolantinoPageWithUrl:(NSString*)pageUrl withWorkflownName:(NSString*)workflowName;
- (void)getVolantinoPageWithUrl:(NSString*)pageUrl withWorkflownName:(NSString*)workflowName completion:(void (^)(UIImage *image))callback;

- (void)getVolantinoPagesUrlWithWorkflowName:(NSString*)workflowName
                             success:(CTXVolantinoSuccessResponseHandler)success
                             failure:(CTXFailedResponseHandler)failure;

- (BOOL)isVolantinoPageDownloaded:(NSString*)pageUrl;

@end
