//
//  UIViewController+CLModal.h
//  CoreLibrary
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <UIKit/UIKit.h>

@interface UIViewController (CLModal)

#pragma mark - Properties
//@property (nonatomic, weak) UIViewController *transparentModalViewController;

- (BOOL)isModal;

/*
- (void)presentTransparentModalViewController: (UIViewController *) aViewController
                                     animated: (BOOL) isAnimated
                                    withAlpha: (CGFloat) anAlpha;

- (void)dismissTransparentModalViewControllerAnimated:(BOOL)animated;
 */

@end
