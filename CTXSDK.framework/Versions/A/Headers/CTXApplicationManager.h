//
//  CTXApplicationManager.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>

#import <CTXSDK/CTXDashboardSettings.h>
#import <CTXSDK/CTXInteraction.h>
#import <CTXSDK/CTXStoryNavigatorSettings.h>
#import <CTXSDK/CTXBaseActionViewController.h>
#import <CTXSDK/CTXGenericTileStyle.h>
#import <CTXSDK/CTXBaseActionStyle.h>

#import <CTXSDK/CTXPushNotificationManager.h>
#import <CTXSDK/CTXInteractionManager.h>
#import <CTXSDK/CoreLibrary.h>

@class CTXStoryNavigatorController;

typedef void (^CTXActionCompletionHandler)(UIViewController *actionViewController);

#pragma mark - Protocol
/**
 *  Tale protocollo deve essere implementato qualora si vogliano customizzare le views scaturite dalle actions. In tal modo è compito del delegato fornire le views customizzate
 */
@protocol CTXApplicationManagerDelegate <NSObject>

@optional

// se si vuole lasciare la visualizzazione degli errori all'App
- (void)showErrorViewWithInfo:(NSError*)error;

// se si vuole lasciare l'implementazione customizzata delle Actions all'App,
// tutte tranne la Notification Action, per la quale c'è un metodo apposito
- (void)actionViewControllerWithType:(C345Action*)action
                            withInfo:(CTXBaseActionInfoResponseBody*)actionInfo
                  withViewController:(UIViewController*)actionViewController
                               error:(NSError*)error
                          completion:(CTXActionCompletionHandler)completion;

- (void)actionTileViewControllerWithBlock:(CTXDashboardBlock*)block
                                 withInfo:(CTXBaseActionInfoResponseBody*)actionInfo
                       withViewController:(UIViewController*)actionViewController
                               completion:(CTXActionCompletionHandler)completion;


- (void)presentLocalNotificationWithTrigger:(CTXTrigger *)trigger;

// se si vuole lasciare la customizzazione della Dashboard all'App
- (CTXDashboardSettings*)settingsForDashboard:(CTXDashboardEntity*)dashboard;


- (void) createStoryNavigatorViewControllerForState: (CTXInteractionState*) state completion: (void(^)(CTXStoryNavigatorController*)) completionHandler;
- (CTXStoryNavigatorSettings*)settingsForStoryNavigator: (CTXStoryNavigatorController*) storyNavigator;

- (int) depthForChannel: (int) channel;

- (int) depthForLinks;

- (void)interactionWillStart:(CTXInteraction*)interaction;
- (void)interactionDidStart:(CTXInteraction*)interaction;

- (void) customizeDefaultTileStyle: (CTXGenericTileStyle*) tileStyle;
- (void) customizeDefaultFullStyle: (CTXBaseActionStyle*) fullStyle;
/**
 Aggiunte Febbraio 2018
 */
/**
 Navigation Controller is provided by App
 */

- (UINavigationController *)provideNavigationControllerForState: (CTXInteractionState *)state entries: (NSArray *)entries forced: (NSArray *)forced;
- (UIImage *)customizeNavigationBackButton;
- (NSString *)titleForNavigationBar;
- (void)customizeNavigationItem:(UINavigationItem *)item;
@end


/**
 *  Si occupa di gestire le embedded UI
 */
@interface CTXApplicationManager : NSObject <CTXInteractionDelegate, CTXPushNotificationDelegate>

#pragma mark - SharedInstance
+ (instancetype)manager;
+ (instancetype)sharedManager;

#pragma mark - Delegates
@property (nonatomic) id<CTXApplicationManagerDelegate> delegate;


#pragma mark - Properties
@property (nonatomic, readonly) UIStoryboard *storyboard;
@property (nonatomic, getter=isInteractionsEnable) BOOL interactionsEnable;
@property (nonatomic, readonly) BOOL isTablet;
@property (nonatomic, readonly) UIViewController *currentVisibleViewController;


- (void)startValidInteraction;
- (void)startInteractionWithId:(NSNumber *)interactionId;
- (void) startFirstValidNoTrigger;


- (void) addState: (CTXInteractionState*) state;
- (void) removeState: (CTXInteractionState*) state;
- (void) resetStates: (CTXInteractionState*) state;

- (void) addMaskedState: (CTXInteractionState*) state;
- (void) removeMaskedState: (CTXInteractionState*) state;
- (void) resetMaskedStates: (CTXInteractionState*) state;

- (CTXStoryNavigatorController *) currentStoryNavigatorForState: (CTXInteractionState*) state;

- (BOOL)showAlertViewWithErrorDescription:(NSString *)errorDescription;






@end
