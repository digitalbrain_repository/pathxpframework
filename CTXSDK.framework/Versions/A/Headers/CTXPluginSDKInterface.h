//
//  CTXPluginSDKInterface.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CLLocation.h>

#import <CTXSDK/C345Request.h>
#import <CTXSDK/C345Response.h>

@class CTXPlugin;

@interface CTXPluginSDKInterface : NSObject

- (instancetype) initWithPlugin: (CTXPlugin*) plugin;

- (NSArray*) cachedActionsForPlugin;

- (void)sendActiveRequest:(C345Request<CTXRequestDelegate>*) request
        completionHandler:(void(^)(C345Response*responseOrNil, NSString*errorMessage)) completionHandler;

- (NSString*)generateTraceID;

- (void) updateAllTriggersWithCompletionHandler: (void(^)(BOOL success)) handler;

- (NSDate*) serverDate;

- (CLLocation *) lastLocationIfAvailable;

@end
