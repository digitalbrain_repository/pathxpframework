//
//  CTXBaseActionViewController.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <UIKit/UIKit.h>

#import <CTXSDK/CTXStoryNavigatorEmbeddableController.h>
#import <CTXSDK/CTXDashboardViewController.h>
#import <CTXSDK/CTXBaseActionStyle.h>
#import <CTXSDK/C345Response.h>

@class CTXInteractionLink;

@interface CTXBaseActionViewController : CTXStoryNavigatorEmbeddableController <CTXBaseActionStylable>

@property (nonatomic) CTXBaseActionStyle * style;

// stylable properties from protocol CTXBaseActionStylable
@property (nonatomic) IBOutletCollection(UIButton) NSMutableArray * stylableClose;
@property (nonatomic) IBOutletCollection(UIView) NSMutableArray * stylableBg;
@property (nonatomic) IBOutletCollection(UILabel) NSMutableArray * stylableTitle;
@property (nonatomic) IBOutletCollection(UIButton) NSMutableArray * stylableButton;
@property (nonatomic,weak) IBOutlet UIView* loaderView;

@property (nonatomic, assign, getter=isClosable) BOOL closable;
@property (nonatomic) IBOutlet UIButton *buttonClose;

/*@property (nonatomic) UIColor * backgroundColor;
@property (nonatomic) UIImage * closeButtonImage;*/

- (UIViewController*) fullParentController;

@property (nonatomic, weak) CTXDashboardViewController *caller;

- (void) closeAndLink: (CTXInteractionLink*) link completion: (void(^)(BOOL, C345Response*)) handler;
- (void) closeAndlinkToInteraction: (NSNumber*) interactionId action: (NSNumber*) actionId completion: (void(^)(BOOL, C345Response*)) handler;
- (BOOL) avoidKenShiro;

- (void) removeMyEntry;
- (IBAction)defaultCloseAction:(id)sender;

@end
