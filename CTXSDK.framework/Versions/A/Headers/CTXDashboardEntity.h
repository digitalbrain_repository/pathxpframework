//
//  CTXDashboardEntity.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <CTXSDK/CLJastor.h>

@class C345Action;


#pragma mark - Class CTXDashboardBlock
@interface CTXDashboardBlock : CLJastor

@property (nonatomic, copy) NSNumber *actionId;
@property (nonatomic, copy) NSString *actionType;
@property (nonatomic, copy) NSString *layout;
@property (nonatomic, copy) NSNumber *position;
@property (nonatomic, copy) NSNumber *width;
@property (nonatomic, copy) NSNumber *height;

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subTitle;
@property (nonatomic, copy) NSNumber *previewImageId;

@property (nonatomic, copy) NSString *titleVerticalPosition;
@property (nonatomic, copy) NSString *titleHorizontalPosition;

@property (nonatomic, copy) NSString *subTitleHorizontalPosition;
@property (nonatomic, copy) NSString *subTitleVerticalPosition;

@property (nonatomic, copy) NSString *titleColor;


- (C345Action*) getEmbeddedAction;

@end

#pragma mark - Class CTXDashboardEntity
@interface CTXDashboardEntity : CLJastor

@property (nonatomic, strong) NSArray *dashboardBlocks;
@property (nonatomic, copy) NSNumber *width;
@property (nonatomic, copy) NSNumber *height;
@property (nonatomic, copy) NSNumber *screenWidth;
@property (nonatomic, copy) NSNumber *screenHeight;

@end
