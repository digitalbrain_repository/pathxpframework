//
//  CTXStoryStackViewController.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <UIKit/UIKit.h>

@class CTXStoryStackViewController;

typedef void(^CTXVoidHandler)(void);
typedef void(^CTXVCCallback)(UIViewController* created, CTXVoidHandler completionHandler);
#define CTXNilVCHandler (handler!=nil)?handler(nil,nil):nil


@protocol CTXStoryStackDataSource <NSObject>

- (void) storyStack: (CTXStoryStackViewController*) stack
viewControllerAfter: (UIViewController*) current
  completionHandler: (CTXVCCallback) handler;

- (void)    storyStack: (CTXStoryStackViewController*) stack
  viewControllerBefore: (UIViewController*) current
     completionHandler: (CTXVCCallback) handler;

@end

@protocol CTXStoryStackDelegate <NSObject>

- (void)        storyStack: (CTXStoryStackViewController*) stack
  switchedToViewController: (UIViewController*) current;

- (void)            storyStack: (CTXStoryStackViewController*) stack
    willSwitchToViewController: (UIViewController*) current
            fromViewController: (UIViewController*) old
             completionHandler: (CTXVoidHandler) handler;



@end

@interface CTXStoryStackViewController : UIViewController

@property (weak, nonatomic) id<CTXStoryStackDataSource> dataSource;
@property (weak, nonatomic) id<CTXStoryStackDelegate> delegate;
@property (nonatomic, readonly) UIViewController * viewController;

- (void) setViewController: (UIViewController*) viewController;
- (void) goNext;
- (void) goPrev;
- (void) reset;

@end
