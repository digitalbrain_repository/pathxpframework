//
//  SocialManager.h
//  ContextApp
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>
#import <Accounts/Accounts.h>
#import <Social/Social.h>

@protocol CTXSocialManagerDelegate <NSObject>

@optional

// Facebook
- (void) didFinishLoginWithFacebookWithToken:(NSString *) token;
- (void) didFinishLoadingProfileInformationForFacebook:(NSDictionary *) info;
- (void) didFailLoginWithFacebookWithError:(NSError *) error;
- (void) didFailLoadingProfileInformationForFacebook:(NSError *) error;

/*
//Twitter
- (void) didFinishLoginWithTwitterWithToken:(NSString *)token secretToken:(NSString *) secret;
- (void) didFailLoginWithTwitterWithError:(NSError *) error;

//Sina Weibo
- (void) didFinishLoginWithSinaWeiboWithToken:(NSString *) token;
- (void) didFailLoginWithSinaWeiboWithError:(NSError *) error;
 */

@end

@interface CTXSocialManager : NSObject

//Facebook
+ (void) loginWithFacebook:(id<CTXSocialManagerDelegate>)delegate;
+ (void) profileForFacebook:(id<CTXSocialManagerDelegate>)delegate;
+ (SLComposeViewController *) shareOnFacebook:(NSDictionary *)info;
+ (void) advancedShareOnFacebook:(NSDictionary *)info;

/*
//Twitter
+ (void) loginWithTwitter:(id<SocialManagerDelegate>)delegate;
+ (SLComposeViewController *) shareOnTwitter:(NSDictionary *)info;

//Sina Weibo
+ (void) loginWithSinaWeibo:(id<SocialManagerDelegate>)delegate;
+ (SLComposeViewController *) shareOnSinaWeibo:(NSDictionary *)info;
 */

@end
