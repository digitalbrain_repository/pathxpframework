//
//  CTXTrigger.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <CTXSDK/CLJastor.h>

typedef NS_ENUM(NSInteger, CTXTriggerType)
{
    CTXTriggerTypeWolverine = 0,
    CTXTriggerTypeBeacon,
    CTXTriggerTypeGeo,
    CTXTriggerTypePush,
    CTXTriggerTypeJust,
    CTXTriggerTypeNo,
    CTXTriggerTypesCount
};

@interface CTXTrigger : CLJastor

@property (nonatomic, copy) NSNumber *interactionId;

@property (nonatomic, copy) NSString *startDate;
@property (nonatomic, copy) NSNumber *startTimezone;

@property (nonatomic, copy) NSString *endDate;
@property (nonatomic, copy) NSNumber *endTimezone;

@property (nonatomic, copy) NSString *notificationText;

@property (nonatomic, readonly) NSDate *startDateFormat;
@property (nonatomic, readonly) NSDate *endDateFormat;

@property (nonatomic) NSArray * interactionStates;

/**
 *  Restituisce TRUE se il trigger è correntemente attivo
 */
@property (nonatomic, readonly, getter = isAvailable) BOOL available;
@property (nonatomic, readonly) CTXTriggerType triggerType;

@end
