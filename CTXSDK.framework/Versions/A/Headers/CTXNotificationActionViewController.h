//
//  CTXNotificationActionViewController.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <UIKit/UIKit.h>
#import <CTXSDK/CoreLibrary.h>
#import <CTXSDK/CTXNotificationActionInfoResponse.h>
#import <CTXSDK/CTXBaseActionViewController.h>
#import <CTXSDK/CTXNotificationActionStyle.h>

#define CTX_NOTIFICATION_ACTION_STORYBOARD    @"CTXNotificationActionStoryboard"
#define CTX_NOTIFICATION_ACTION_VC_IDENTIFIER     @"CTXNotificationActionVC"

@interface CTXNotificationActionViewController : CTXBaseActionViewController <CTXNotificationActionStylable>

#pragma mark - Properties
@property (nonatomic, readonly) UIWindow *notificationWindow;

// tali proprietà sono mutuamente esclusive. Prioritario è notificationInfo
@property (nonatomic) CTXNotificationActionInfoResponseBody *notificationInfo;
@property (nonatomic) NSNumber *interactionId;

@property (nonatomic) NSString *notificationText;

@property (nonatomic, copy) VoidBlock dismissHandler;

#pragma mark - Customization Properties
@property (nonatomic) CGFloat notificationHeight;
@property (nonatomic) NSTimeInterval notificationTimeInterval;

@property (nonatomic) IBOutletCollection(UIView) NSMutableArray * stylableSeparator;


#pragma mark - Functions
- (void)show;
- (void)hide;

@end
