//
//  CTXEventsManager.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>
#import <CTXSDK/CTXEventsResponse.h>
#import <CTXSDK/CTXServiceManager.h>

typedef void (^CTXEventsSuccessResponseHandler)(CTXEventsResponse *eventsResponse);

@interface CTXEventsManager : NSObject

#pragma mark - Shared Instance
+ (instancetype)manager;

#pragma mark - Functions
- (void)getEventsWithWorkflowName:(NSString*)workflowName
                            limit:(NSUInteger)limit
                         latitude:(double)latitude
                        longitude:(double)longitude
                          success:(CTXEventsSuccessResponseHandler)success
                          failure:(CTXFailedResponseHandler)failure;

- (void)getEventsWithWorkflowName:(NSString*)workflowName
                         latitude:(double)latitude
                        longitude:(double)longitude
                            nelng:(double)nelng
                            nelat:(double)nelat
                            swlng:(double)swlng
                            swlat:(double)swlat
                          success:(CTXEventsSuccessResponseHandler)success
                          failure:(CTXFailedResponseHandler)failure;




- (void)getAllEvents:(NSString*)workflowName success:(CTXEventsSuccessResponseHandler)success failure:(CTXFailedResponseHandler)failure;


-(void)getSingleEventById:(NSString *)idEvent success:(CTXEventsSuccessResponseHandler)success failure:(CTXFailedResponseHandler)failure;

@end