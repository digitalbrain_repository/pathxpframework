//
//  ContextBaseResponse.h
//  CoreLibrary
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <CTXSDK/CLJastor.h>

@class CTXResponseBody;

#pragma mark - Protocol CTXResponseDelegate
@protocol CTXResponseDelegate <NSObject>

@required
#pragma mark - Properties
@property (nonatomic) CTXResponseBody *_Nullable body;

@end

#pragma mark - Class CTXResponseStatus
@interface CTXResponseStatus : CLJastor

#pragma mark - Properties
@property (nonatomic) NSInteger statusCode;
@property (nonatomic) NSString *_Nullable statusDescription;

@end

#pragma mark - Class CTXResponseHeader
@interface CTXResponseHeader : CLJastor

#pragma mark - Properties
@property (nonatomic) NSString *_Nullable traceId;
@property (nonatomic) CTXResponseStatus *_Nullable responseStatus;

@end

#pragma mark - Class CTXResponseError
@interface CTXResponseError : CLJastor

#pragma mark - Properties
@property (nonatomic) NSString *_Nullable code;
@property (nonatomic) NSString *_Nullable descript;

@end

#pragma mark - Class CTXResponseBody
@interface CTXResponseBody : CLJastor

#pragma mark - Properties
@property (nonatomic) CTXResponseError *_Nullable error;

@end

#pragma mark - Class C345Response
@interface C345Response : CLJastor

#pragma mark - Properties
@property (nonatomic) CTXResponseHeader *_Nullable responseHeader;
@property (nonatomic) CTXResponseBody *_Nullable body;
@property (nonatomic) NSString *_Nullable errorMessage;

#pragma mark - Initializer
- (instancetype _Nonnull)initWithErrorMessage:(NSString*_Nullable)error;

@end
