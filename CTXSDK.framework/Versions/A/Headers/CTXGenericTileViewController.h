//
//  CTXGenericTileViewController.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <CTXSDK/CTXDashboardTileBaseViewController.h>
#import <CTXSDK/CTXBaseActionInfoResponseBody.h>
#import <CTXSDK/CTXGenericTileStyle.h>
#import <CTXSDK/FLAnimatedImageView.h>
#import <CTXSDK/CTXTileTitleView.h>
@interface CTXGenericTileViewController : CTXDashboardTileBaseViewController

@property (nonatomic) CTXBaseActionInfoResponseBody * actionInfo;

@property (nonatomic, weak) IBOutlet CTXTileTitleView * titleView;
@property (nonatomic, weak) IBOutlet FLAnimatedImageView * imageView;


- (void)designView;
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer;
- (UIView *)createViewForAnimation ;
- (void) bringToFullscreen;
@end
