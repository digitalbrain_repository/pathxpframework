//
//  ContextLoginResponse.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>
#import <CTXSDK/C345Response.h>

typedef NS_ENUM(NSInteger, CTXGenderType)
{
    CTXGenderTypeMale = 0,
    CTXGenderTypeFemale
};

@class CTXLoginResponse;

#pragma mark - Defines
typedef void (^CTXLoginSuccessResponseHandler)(CTXLoginResponse *loginResponse);

#pragma mark - Class CTXLoginResponseBody
@interface CTXLoginResponseBody : CTXResponseBody

#pragma mark - Properties
@property (nonatomic) NSString *additionalInfo1;
@property (nonatomic) NSString *additionalInfo2;
@property (nonatomic) NSString *additionalInfo3;
@property (nonatomic) NSDictionary *additionalInfo4;
@property (nonatomic) NSString *message;
@property (nonatomic) NSNumber *skinId;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *surname;
@property (nonatomic) NSString *birthday;
@property (nonatomic) NSString *gender;
@property (nonatomic) NSString *email;

#pragma mark - Properties ReadOnly
@property (nonatomic, readonly) NSDate *birthdayDate;
@property (nonatomic, readonly) CTXGenderType genderType;

@end

#pragma mark - Class CTXLoginResponse
@interface CTXLoginResponse : C345Response

#pragma mark - Properties
@property (nonatomic) CTXLoginResponseBody *body;

@end
