//
//  CTXUpdateManager.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>

@interface CTXUpdateManager : NSObject

#pragma mark - Shared Instance
+ (instancetype)manager;

- (void)startTriggersUpdate;
- (void)startCacheUpdate;

- (void) updateAllTriggersWithCompletionHandler: (void(^)(BOOL success)) handler;

@end
