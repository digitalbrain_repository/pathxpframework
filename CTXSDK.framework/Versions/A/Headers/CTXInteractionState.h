//
//  CTXInteractionState.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>

#import <CTXSDK/CLJastor.h>

typedef NS_ENUM(NSInteger, CTXInteractionStateType)
{
    CTXInteractionStateTypeListening    = 0,
    CTXInteractionStateTypeCustom
};

@interface CTXInteractionState : CLJastor

@property (nonatomic, copy) NSString * state;
@property (nonatomic, copy) NSString * param;
@property (readonly) NSString * stateKey;

@property (nonatomic, readonly) CTXInteractionStateType type;

+ (instancetype) defaultState;
+ (instancetype) customStateWithParams: (NSString*) params;

@end
