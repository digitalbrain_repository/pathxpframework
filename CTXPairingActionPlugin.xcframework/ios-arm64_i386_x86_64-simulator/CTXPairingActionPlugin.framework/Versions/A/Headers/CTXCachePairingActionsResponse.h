//
//  CTXCachePairingActionsResponse.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <CTXSDK/C345Response.h>
#import <CTXSDK/CTXCacheBaseActionsResponseBody.h>

@interface CTXCachePairingActionsResponseBody : CTXCacheBaseActionsResponseBody

#pragma mark - Properties
@property (nonatomic, strong) NSArray *pairingActions;

@end

@interface CTXCachePairingActionsResponse : C345Response

#pragma mark - Properties
@property (nonatomic) CTXCachePairingActionsResponseBody *body;

@end
