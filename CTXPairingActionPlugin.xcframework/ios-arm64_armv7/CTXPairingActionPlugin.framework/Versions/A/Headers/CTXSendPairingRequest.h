//
//  CTXSendPairingRequest.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <CTXSDK/C345Request.h>

@interface CTXSendPairingRequestBody : CTXRequestBody

#pragma mark - Properties
@property (nonatomic) NSString *beaconMacAddress;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *surname;
@property (nonatomic) NSString *phone;
@property (nonatomic) NSString *email;
@property (nonatomic) NSArray *terms;

@end

@interface CTXSendPairingRequest : C345Request <CTXRequestDelegate>

#pragma mark - Inizializer
- (instancetype)initWithTraceID:(NSString*)traceID;

@end
