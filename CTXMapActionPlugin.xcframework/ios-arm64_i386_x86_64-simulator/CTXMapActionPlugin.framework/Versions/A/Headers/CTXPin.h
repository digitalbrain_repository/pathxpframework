//
//  CTXPin.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <CTXSDK/CLJastor.h>

@interface CTXPin : CLJastor

@property (nonatomic) NSNumber *lat;
@property (nonatomic) NSNumber *lon;

@end
