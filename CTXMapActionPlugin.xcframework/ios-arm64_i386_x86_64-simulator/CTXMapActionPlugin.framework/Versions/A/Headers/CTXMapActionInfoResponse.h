//
//  CTXMapActionInfoResponse.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//
#import <UIKit/UIKit.h>
#import <CTXSDK/C345Response.h>
#import <CTXSDK/CTXBaseActionInfoResponseBody.h>
#import <CTXMapActionPlugin/CTXBoundingBox.h>
#import <CTXMapActionPlugin/CTXPin.h>

#pragma mark - Class CTXMapActionInfoResponseBody
@interface CTXMapActionInfoResponseBody : CTXBaseActionInfoResponseBody

#pragma mark - Properties
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *text;
@property (nonatomic) CTXBoundingBox *boundingbox;
@property (nonatomic) CTXPin *pin;
@property (nonatomic) NSString *infowindow;
@property (nonatomic) NSString *colour;
@property (nonatomic) NSNumber *pinImageId;
@property (nonatomic) NSNumber *logoImageId;
@property (nonatomic) NSNumber *logoImageIsHeader;
#pragma mark - Custom Properties
//@property (nonatomic) UIImage *pinImage;
//@property (nonatomic) UIImage *logoImage;

@end

#pragma mark - Class CTXMapActionInfoResponse
@interface CTXMapActionInfoResponse : C345Response

#pragma mark - Properties
@property (nonatomic) CTXMapActionInfoResponseBody *body;

@end
