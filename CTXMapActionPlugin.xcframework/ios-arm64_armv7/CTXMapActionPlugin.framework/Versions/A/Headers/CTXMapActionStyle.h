//
//  CTXMapActionStyle.h
//  MapActionPlugin
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <CTXSDK/CTXBaseActionStyle.h>

@protocol CTXMapActionStylable <CTXBaseActionStylable>

@property (nonatomic) IBOutletCollection(UIImageView) NSMutableArray * stylableMarkerAddress;
@property (nonatomic) IBOutletCollection(UILabel) NSMutableArray * stylableAddress;

@end

@interface CTXMapActionStyle : CTXBaseActionStyle

@property (nonatomic) CTXImageResource * poiMarkerAddress;

@property (nonatomic) UIColor * addressTextColor;
@property (nonatomic) CTXFontResource * addressFont;

@property (nonatomic) NSString * buttonText;

-(void)applyTo:(id<CTXMapActionStylable>)stylable;

@end
