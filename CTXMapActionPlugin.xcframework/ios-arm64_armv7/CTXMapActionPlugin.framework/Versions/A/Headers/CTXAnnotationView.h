//
//  CTXAnnotationView.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <MapKit/MapKit.h>

#define CTX_ANNOTATION_VIEW_IDENTIFIER  @"CTXAnnotationViewIdentifier"

@interface CTXAnnotationView : MKAnnotationView

@end
