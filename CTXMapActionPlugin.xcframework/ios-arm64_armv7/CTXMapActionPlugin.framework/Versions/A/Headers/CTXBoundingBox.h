//
//  CTXBoundingBox.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <CTXSDK/CLJastor.h>

@interface CTXBoundingBox : CLJastor

@property (nonatomic) NSNumber *lat1;
@property (nonatomic) NSNumber *lon1;
@property (nonatomic) NSNumber *lat2;
@property (nonatomic) NSNumber *lon2;

@end
