//
//  CTXFormFieldCell.h
//  FormActionPlugin
//
//  Created by Massimiliano Schinco on 16/01/17.
//  Copyright © 2017 Massimiliano Schinco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CTXFormActionPlugin/CTXFormActionInfoResponse.h>
#import <CTXSDK/CTXActionPlugin.h>

@protocol CTXFormFieldDelegate <NSObject>
- (void)formFieldCellDidBeginEditingAtIndexPath:(NSIndexPath *)indexPath;

@end

@interface CTXFormFieldCell : UITableViewCell <UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
@property (nonatomic,weak) IBOutlet UILabel *nameLbl;
@property (nonatomic,weak) IBOutlet UITextField *fieldLbl;
@property (nonatomic,weak) id <CTXFormFieldDelegate> delegate;
@property (nonatomic,weak) CTXFormItem * item;
@property (nonatomic,strong) NSIndexPath *indexPath;
@property (nonatomic,strong) NSDateFormatter *displayDateFormat;
@property (nonatomic,strong) NSDateFormatter *serviceDateFormat;
@property (nonatomic,strong) UIView *inputAccessoryViewForField;
@property (nonatomic, weak) CTXActionPlugin * responsiblePlugin;
//- (void)setItem: (CTXFormItem *)item;
@end
