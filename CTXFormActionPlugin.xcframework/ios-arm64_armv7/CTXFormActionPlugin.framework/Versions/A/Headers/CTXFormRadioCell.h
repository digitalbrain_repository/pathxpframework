//
//  CTXFormRadioCell.h
//  FormActionPlugin
//
//  Created by Massimiliano on 09/02/17.
//  Copyright © 2017 Massimiliano Schinco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CTXFormActionPlugin/CTXFormFieldCell.h>

@interface CTXFormRadioCell : CTXFormFieldCell
@property (nonatomic, strong) NSMutableArray *buttons;
@property (nonatomic,weak) IBOutlet UIView *radiosContainer;
@end
