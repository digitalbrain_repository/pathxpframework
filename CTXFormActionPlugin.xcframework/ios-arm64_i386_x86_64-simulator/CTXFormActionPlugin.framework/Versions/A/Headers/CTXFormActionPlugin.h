//
//  CTXFormActionPlugin.h
//  FormActionPlugin
//
//  Created by Massimiliano Schinco on 16/01/17.
//  Copyright © 2017 Massimiliano Schinco. All rights reserved.
//

#import <CTXSDK/CTXActionPlugin.h>

@interface CTXFormActionPlugin : CTXActionPlugin

@end
