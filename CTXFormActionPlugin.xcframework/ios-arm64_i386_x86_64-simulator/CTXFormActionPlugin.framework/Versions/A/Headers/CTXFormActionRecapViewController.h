//
//  CTXFormActionRecapViewController.h
//  CTXFormActionPlugin
//
//  Created by Massimiliano on 28/08/18.
//  Copyright © 2018 Massimiliano Schinco. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <CTXFormActionPlugin/CTXFormActionInfoResponse.h>
@interface CTXFormActionRecapViewController : UIViewController
@property (nonatomic,strong) NSArray <CTXFormItem *> *form;
- (void)reload;
@end
