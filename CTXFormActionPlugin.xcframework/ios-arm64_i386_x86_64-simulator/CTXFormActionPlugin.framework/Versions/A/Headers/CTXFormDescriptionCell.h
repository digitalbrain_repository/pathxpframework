//
//  CTXFormDescriptionCell.h
//  FormActionPlugin
//
//  Created by Massimiliano on 09/02/17.
//  Copyright © 2017 Massimiliano Schinco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CTXFormActionPlugin/CTXFormFieldCell.h>
#import <CTXSDK/CLAdvancedAttributedLabel.h>

@interface CTXFormDescriptionCell : CTXFormFieldCell <CLAdvancedAttributedLabelDelegate>
@property (nonatomic, weak) IBOutlet CLAdvancedAttributedLabel *descriptionLbl;
@property (nonatomic, weak) IBOutlet CLAdvancedAttributedLabel *nameHtmlLbl;
@property (nonatomic,strong) NSString *htmlStyleStr;
@end
