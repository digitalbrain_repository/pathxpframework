//
//  CTXTimelineActionStyle.h
//  TimelineActionPlugin
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <CTXSDK/CTXBaseActionStyle.h>

@protocol CTXTimelineActionStylable <CTXBaseActionStylable>

@property (nonatomic) IBOutletCollection(UIView) NSMutableArray * stylableFrame;

@property (nonatomic) IBOutletCollection(UIImageView) NSMutableArray * stylableHeader;
@property (nonatomic) IBOutletCollection(UIImageView) NSMutableArray * stylableClockIco;

@property (nonatomic) IBOutletCollection(UILabel) NSMutableArray * stylableCardTitle;
@property (nonatomic) IBOutletCollection(UILabel) NSMutableArray * stylableCardDescription;
@property (nonatomic) IBOutletCollection(UILabel) NSMutableArray * stylableTime;
@property (nonatomic) IBOutletCollection(UILabel) NSMutableArray * stylableDate;


@end

@interface CTXTimelineActionStyle : CTXBaseActionStyle

@property (nonatomic) CTXImageResource * headerImage;
@property (nonatomic) CTXImageResource * clockIco;

@property (nonatomic) UIColor * cardTitleTextColor;
@property (nonatomic) CTXFontResource * cardTitleFont;

@property (nonatomic) UIColor * cardDescriptionTextColor;
@property (nonatomic) CTXFontResource * cardDescriptionFont;

@property (nonatomic) UIColor * timeTextColor;
@property (nonatomic) CTXFontResource * timeFont;

@property (nonatomic) UIColor * dateTextColor;
@property (nonatomic) CTXFontResource * dateFont;


@property (nonatomic) UIColor * frameBgColor;

-(void)applyTo:(id<CTXTimelineActionStylable>)stylable;

@end
