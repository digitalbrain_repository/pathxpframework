//
//  CTXTimelineFlowLayout.h
//  TimelineActionPlugin
//
//  Created by Massimiliano Schinco on 28/07/16.
//  Copyright © 2016 com.mxm345. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CTXTimelineFlowLayout : UICollectionViewFlowLayout
@property (nonatomic) BOOL pagingEnabled;
@end
