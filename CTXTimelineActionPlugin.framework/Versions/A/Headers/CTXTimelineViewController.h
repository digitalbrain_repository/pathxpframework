//
//  CTXTimelineViewController.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <UIKit/UIKit.h>
#import <CTXTimelineActionPlugin/CTXTimelineActionInfoResponse.h>
#import <CTXSDK/CTXDashboardTileBaseViewController.h>
#import <CTXTimelineActionPlugin/CTXTimelineTileStyle.h>
#import <CTXTimelineActionPlugin/CTXCollectionViewIndicatorView.h>

@interface CTXTimelineViewController : CTXDashboardTileBaseViewController <UICollectionViewDataSource,UICollectionViewDelegate,UIScrollViewDelegate>
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UILabel *dateEventLbl;
@property (nonatomic, weak) IBOutlet CTXCollectionViewIndicatorView *indicatorScrollView;
@property (nonatomic, weak) IBOutlet CTXScrollViewCustomPage *fakeScrollView;
@property (nonatomic) CTXTimelineActionInfoResponseBody *timelineInfo;

#pragma mark - SOLO MINI E SMALL
@property (nonatomic,weak) IBOutlet UIImageView *miniEventImg; //immagine di sfondo
@property (weak, nonatomic) IBOutlet UILabel *labelTitle; //label che viene visualizzata se la tile è mini o small
@property (weak, nonatomic) IBOutlet UILabel *labelSubTitle; //label che viene visualizzata se la tile è mini o small

#pragma mark - SOLO PER MEDIUM
@property (nonatomic,weak) IBOutlet UIView *animationView;
@property (nonatomic,weak) IBOutlet UIImageView *animationImgView;

@end
