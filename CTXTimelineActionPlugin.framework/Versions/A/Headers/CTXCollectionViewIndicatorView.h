//
//  CTXCollectionViewIndicatorView.h
//  TimelineActionPlugin
//
//  Created by Massimiliano Schinco on 29/07/16.
//  Copyright © 2016 com.mxm345. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CTXSDK/CTXActionPlugin.h>

@interface CTXScrollViewCustomPage : UIScrollView
@property (nonatomic,weak) IBOutlet UIScrollView *forwardScrollView;
- (void)setContentOffsetWithoutForward:(CGPoint)contentOffset;
@end


@interface CTXCollectionViewIndicatorView : CTXScrollViewCustomPage <UIScrollViewDelegate>

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) CTXActionPlugin *responsibleActionPlugin;
@property (nonatomic, strong) NSArray *titleForSections;
@end 
