//
//  CTXTimelineActionResponse.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//
#import <UIKit/UIKit.h>
#import <CTXSDK/C345Response.h>
#import <CTXSDK/CTXBaseActionInfoResponseBody.h>

#pragma mark - Class CTXLocationPoint
@interface CTXLocationPoint : CLJastor

@property (nonatomic) NSNumber *lat;
@property (nonatomic) NSNumber *lon;

@end

#pragma mark - Class CTXTimelineEvent
@interface CTXTimelineEvent : CLJastor

#pragma mark - Properties
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *descript;
@property (nonatomic) CTXLocationPoint *locationPoint;
@property (nonatomic) NSString *locationAddress;
@property (nonatomic) NSString *tag;
@property (nonatomic) NSString *time;
@property (nonatomic) NSString *endTime;
@property (nonatomic) BOOL presentInTimeline;
@property (nonatomic) NSNumber *imageId;

#pragma mark - Custom Properties
@property (nonatomic, readonly) NSDate *timeDate;
@property (nonatomic, readonly) NSDate *endTimeDate;
//@property (nonatomic) UIImage *image;

@end

#pragma mark - Class CTXTimelineActionResponseBody
@interface CTXTimelineActionInfoResponseBody : CTXBaseActionInfoResponseBody

#pragma mark - Properties
@property (nonatomic) NSString *title;
@property (nonatomic) NSNumber *imageId;
@property (nonatomic) NSArray *timelineEvents;

#pragma mark - Custom Properties
//@property (nonatomic) UIImage *image;

@end

#pragma mark - Class CTXTimelineActionResponse
@interface CTXTimelineActionInfoResponse : C345Response

#pragma mark - Properties
@property (nonatomic) CTXTimelineActionInfoResponseBody *body;

@end
