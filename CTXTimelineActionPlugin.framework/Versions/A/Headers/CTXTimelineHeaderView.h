//
//  CTXTimelineHeaderView.h
//  TimelineActionPlugin
//
//  Created by Massimiliano Schinco on 01/08/16.
//  Copyright © 2016 com.mxm345. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CTXTimelineHeaderView : UIView
@property (nonatomic, weak) IBOutlet UILabel *titleLbl;

@end
