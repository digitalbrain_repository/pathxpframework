//
//  CTXTimelineActionPlugin.h
//  DefaultPlugins
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <CTXSDK/CTXActionPlugin.h>

#import <CTXTimelineActionPlugin/CTXTimelineActionStyle.h>
#import <CTXTimelineActionPlugin/CTXTimelineTileStyle.h>


@interface CTXTimelineActionPlugin : CTXActionPlugin

-(CTXTimelineActionStyle *)fullStyle;
-(CTXTimelineTileStyle *)tileStyle;

@end
