//
//  CTXHubActionResponse.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <UIKit/UIKit.h>
#import <CTXSDK/C345Response.h>
#import <CTXSDK/CTXBaseActionInfoResponseBody.h>

#pragma mark - Class CTXHubActionResponseBody
@interface CTXDotStyle: CLJastor
@property (nonatomic) NSString *color ;
@property (nonatomic) NSString *position;
@property (nonatomic) NSString *selectedColor;
@end
@interface CTXHubActionInfoResponseBody : CTXBaseActionInfoResponseBody

#pragma mark - Properties
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *subTitle;
@property (nonatomic) NSNumber *previewImageId;
@property (nonatomic) NSNumber *headerImageId;
@property (nonatomic) NSString *callToAction;
@property (nonatomic) NSInteger events;
@property (nonatomic) NSString *text;
@property (nonatomic) NSString *info;
@property (nonatomic) NSInteger blocks;
@property (nonatomic) UIColor *themeColor;
@property (nonatomic) NSArray *media;
@property (nonatomic) CTXDotStyle *dots;

@end

#pragma mark - Class CTXHubActionResponse
@interface CTXHubActionInfoResponse : C345Response

#pragma mark - Properties
@property (nonatomic) CTXHubActionInfoResponseBody *body;

@end
