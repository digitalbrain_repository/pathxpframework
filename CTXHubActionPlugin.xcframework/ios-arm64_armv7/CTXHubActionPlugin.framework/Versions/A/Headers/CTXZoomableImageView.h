//
//  DBZoomableImageView.h
//  DBComponents
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <UIKit/UIKit.h>

@interface CTXZoomableImageView : UIScrollView <UIScrollViewDelegate>
{
    
}
@property (nonatomic, strong) UIImageView *imageView;

@property (nonatomic, strong) UIImage *image;

@property (nonatomic) UIViewContentMode contentMode;


@end
