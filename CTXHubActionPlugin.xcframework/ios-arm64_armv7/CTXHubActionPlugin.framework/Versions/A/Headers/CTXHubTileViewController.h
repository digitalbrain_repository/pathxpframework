//
//  CTXHubTileViewController.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <CTXSDK/CTXGenericTileViewController.h>
#import <CTXHubActionPlugin/CTXHubActionInfoResponse.h>
#import <CTXHubActionPlugin/CTXHubTileStyle.h>

#define CTX_HUB_TILE_VIEWCONTROLLER_IDENTIFIER   @"CTXHubTileVCIdentifier"

@interface CTXHubTileViewController : CTXGenericTileViewController <CTXHubTileStylable>

@property (nonatomic) IBOutletCollection(UILabel) NSMutableArray * stylableInfo;
@property (nonatomic) IBOutletCollection(UIView) NSMutableArray * stylableTextContainer;
@property (nonatomic) IBOutletCollection(UIImageView) NSMutableArray * stylableTextContainerShadow;


@end
