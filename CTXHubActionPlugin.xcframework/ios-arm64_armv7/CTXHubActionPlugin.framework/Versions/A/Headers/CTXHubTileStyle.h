//
//  CTXHubTileStyle.h
//  HubActionPlugin
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <CTXSDK/CTXGenericTileStyle.h>

@protocol CTXHubTileStylable <CTXGenericTileStylable>

@property (nonatomic) IBOutletCollection(UILabel) NSMutableArray * stylableInfo;
@property (nonatomic) IBOutletCollection(UIView) NSMutableArray * stylableTextContainer;
@property (nonatomic) IBOutletCollection(UIImageView) NSMutableArray * stylableTextContainerShadow;

@end

@interface CTXHubTileStyle : CTXGenericTileStyle

@property (nonatomic) UIColor * textContainerBgColor;
@property (nonatomic) CTXImageResource * textContainerShadow;

@property (nonatomic) CTXFontResource * infoFont;
@property (nonatomic) UIColor * infoTextColor;

-(void)applyTo:(id<CTXHubTileStylable>)stylable;

@end
