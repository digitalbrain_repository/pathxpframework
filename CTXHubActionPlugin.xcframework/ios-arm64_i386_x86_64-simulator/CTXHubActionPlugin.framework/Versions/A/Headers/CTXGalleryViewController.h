//
//  DBGalleryFullScreenView.h
//  DBComponents
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <UIKit/UIKit.h>
typedef UIImage * (^DBGalleryVIewControllerImageAtIndex)(NSUInteger );
typedef void(^DBGalleryWillDismiss)(NSUInteger page, UIImageView *galleryImageView);
typedef void(^DBGalleryDidDismiss)(NSUInteger page , UIImageView *galleryImageView);
typedef void(^DBGalleryDidChangePage)(NSUInteger page);
typedef void(^DBGalleryConfigureImageAtIndex)(NSUInteger page,UIImageView *imageView);
@interface CTXGalleryViewController : UIViewController <UIScrollViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UIGestureRecognizerDelegate,UIApplicationDelegate>
{
    UIImageView *screenImageView;
    UIImageView *senderImageView;
    UIView *backgroundView;
    CGPoint initialTouchPoint;
    UIButton *closeBtn;
}
@property (nonatomic) NSUInteger currentPage;
@property (nonatomic) CGFloat minimumZoomScale;
@property (nonatomic) CGFloat maximumZoomScale;
@property (nonatomic) NSUInteger numberOfPages;
@property (nonatomic,retain) UICollectionView *collectionView;
@property (nonatomic,copy) DBGalleryVIewControllerImageAtIndex imageAtIndex;
@property (nonatomic,copy) DBGalleryDidDismiss didDismissHandler;
@property (nonatomic,copy) DBGalleryDidDismiss willDismissHandler;
@property (nonatomic,copy) DBGalleryDidChangePage didChangePage;
@property (nonatomic,copy) DBGalleryConfigureImageAtIndex configureImageAtIndex;

-(void)showFromImageView:(UIImageView *)aSenderImageView ;
@end
