//
//  CTXHubActionStyle.h
//  HubActionPlugin
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <CTXSDK/CTXBaseActionStyle.h>

@protocol CTXHubActionStylable <CTXBaseActionStylable>

@property (nonatomic) IBOutletCollection(UILabel) NSMutableArray * stylableText;
@property (nonatomic) IBOutletCollection(UILabel) NSMutableArray * stylableInfo;
@property (nonatomic) IBOutletCollection(UIPageControl) NSMutableArray * stylablePageControl;
@property (nonatomic) IBOutletCollection(UIView) NSMutableArray * stylablePageControlBackground;


@end


@interface CTXHubActionStyle : CTXBaseActionStyle

@property (nonatomic) UIColor * textColor;
@property (nonatomic) UIColor * infoColor;

@property (nonatomic) CTXFontResource * textFont;
@property (nonatomic) CTXFontResource * infoFont;

@property (nonatomic) UIColor * circleSelectorBgColor;
@property (nonatomic) UIColor * circleSelectorColor;
@property (nonatomic) UIColor * circleSelectorSelectedColor;

-(void)applyTo:(id<CTXHubActionStylable>)stylable;

@end
