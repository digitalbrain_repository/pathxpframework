//
//  CTXHubFullViewController.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <CTXSDK/CTXBaseActionViewController.h>
#import <CTXHubActionPlugin/CTXHubActionInfoResponse.h>
#import <CTXHubActionPlugin/CTXHubActionStyle.h>

#define CTX_HUB_STORYBOARD_NAME     @"CTXHubStoryboard"
#define CTX_HUB_FULL_VIEWCONTROLLER_IDENTIFIER  @"CTXHubFullVCIdentifier"

@interface CTXHubFullViewController : CTXBaseActionViewController <CTXHubActionStylable>

@property (nonatomic) CTXHubActionInfoResponseBody *hubInfo;

@property (nonatomic) IBOutletCollection(UILabel) NSMutableArray * stylableText;
@property (nonatomic) IBOutletCollection(UILabel) NSMutableArray * stylableInfo;
@property (nonatomic) IBOutletCollection(UIPageControl) NSMutableArray * stylablePageControl;
@property (nonatomic) IBOutletCollection(UIView) NSMutableArray * stylablePageControlBackground;


@end
