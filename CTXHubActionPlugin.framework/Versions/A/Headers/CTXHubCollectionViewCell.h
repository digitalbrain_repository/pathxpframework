//
//  CTXHubCollectionViewCell.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <UIKit/UIKit.h>

#define CTX_HUB_COLLECTION_VIEW_CELL_IDENTIFIER     @"CTXHubCollectionViewCellIdentifier"



@interface CTXHubCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;

- (void)setInfo:(UIImage *)cellInfo;

@end
