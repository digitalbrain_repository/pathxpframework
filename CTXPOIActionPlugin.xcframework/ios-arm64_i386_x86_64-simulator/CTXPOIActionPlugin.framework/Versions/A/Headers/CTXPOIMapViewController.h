//
//  CTXPOIMapViewController.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <UIKit/UIKit.h>
#import <CTXPOIActionPlugin/CTXPOIActionInfoResponse.h>
#import <CTXPOIActionPlugin/CTXPOIFullViewController.h>
#import <CTXPOIActionPlugin/CTXPOIActionStyle.h>

#define CTX_POI_MAP_VC_IDENTIFIER  @"CTXPOIMapVCIdentifier"

@class CTXActionPlugin;

@interface CTXPOIMapViewController : UIViewController <CTXPOIActionStylable>

#pragma mark - Properties
@property (nonatomic) id<CTXPOIDelegate> delegate;
@property (nonatomic) CTXPOIActionInfoResponseBody *poiInfo;
@property (nonatomic, weak) CTXActionPlugin * responsiblePlugin;

@property (nonatomic, weak) CTXPOIActionStyle * style;

@property (nonatomic) IBOutletCollection(UIView) NSMutableArray* stylableFrame;



@end
