//
//  CTXPOIFullViewController.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <CTXSDK/CTXBaseActionViewController.h>
#import <CTXPOIActionPlugin/CTXPOIActionInfoResponse.h>
#import <CTXPOIActionPlugin/CTXPOI.h>
#import <CTXPOIActionPlugin/CTXPOIActionStyle.h>

#define CTX_POI_STORYBOARD_NAME     @"CTXPOIStoryboard"
#define CTX_POI_FULL_VIEWCONTROLLER_IDENTIFIER  @"CTXPOIFullVCIdentifier"

@protocol CTXPOIDelegate <NSObject>

- (void)displayViewController:(UIViewController*)viewController didSelectPOI:(CTXPOI *)selectedPOI;

@end

@interface CTXPOIFullViewController : CTXBaseActionViewController <CTXPOIDelegate, CTXPOIActionStylable>

#pragma mark - Properties
@property (nonatomic) CTXPOIActionInfoResponseBody *poiInfo;

@property (nonatomic) IBOutletCollection(UIView) NSMutableArray* stylableFrame;
@property (nonatomic) IBOutletCollection(UIButton) NSMutableArray* stylableTabMapButton;
@property (nonatomic) IBOutletCollection(UIButton) NSMutableArray* stylableTabListButton;
@property (nonatomic) IBOutletCollection(UIImageView) NSMutableArray* stylableVideoLinkIcon;
@property (nonatomic) IBOutletCollection(UILabel) NSMutableArray* stylableVideoLinkText;


@end
