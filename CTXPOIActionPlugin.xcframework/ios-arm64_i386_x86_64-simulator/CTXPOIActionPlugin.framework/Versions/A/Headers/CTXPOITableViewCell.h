//
//  CTXPOITableViewCell.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <UIKit/UIKit.h>
#import <CTXPOIActionPlugin/CTXPOI.h>
#import <CTXPOIActionPlugin/CTXPOIActionStyle.h>

#define CTX_POI_VIEW_CELL_IDENTIFIER    @"CTXPOITableViewCellIdentifier"

@interface CTXPOITableViewCell : UITableViewCell <CTXPOIActionStylable>

@property (nonatomic) CTXPOI *selectedPOIInfo;

// style:

@property (nonatomic, weak) CTXPOIActionStyle * style;

@property (nonatomic) IBOutletCollection(UILabel) NSMutableArray* stylableListText;
@property (nonatomic) IBOutletCollection(UIImageView) NSMutableArray* stylableListArrow;

@property (nonatomic) IBOutletCollection(UIView) NSMutableArray* stylableListShadow;
@property (nonatomic) IBOutletCollection(UIImageView) NSMutableArray* stylableListShadowMarker;
@property (nonatomic) IBOutletCollection(UILabel) NSMutableArray* stylableListShadowText;
-(void)setSelectedPOIInfo:(CTXPOI *)selectedPOIInfo withCurrentLocation:(CLLocation*)currentLocation;
@end
