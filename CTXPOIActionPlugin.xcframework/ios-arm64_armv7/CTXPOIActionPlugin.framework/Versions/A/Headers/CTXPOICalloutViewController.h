//
//  CTXPOICalloutViewController.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <UIKit/UIKit.h>
#import <CTXPOIActionPlugin/CTXPOI.h>
#import <CTXPOIActionPlugin/CTXPOIActionInfoResponse.h>
#import <CTXPOIActionPlugin/CTXPOIFullViewController.h>
#import <CTXPOIActionPlugin/CTXPOIActionStyle.h>

#define CTX_POI_CALLOUT_IDENTIFIER  @"CTXPOICalloutVCIdentifier"

@interface CTXPOICalloutViewController : UIViewController <CTXPOIActionStylable>

@property (nonatomic) CTXPOI *selectedPOI;
@property (nonatomic) CTXPOIActionInfoResponseBody *poiInfo;
@property (nonatomic) id<CTXPOIDelegate> delegate;

@property (nonatomic) IBOutletCollection(UIView) NSMutableArray* stylableMapPoiFrame;
@property (nonatomic) IBOutletCollection(UIButton) NSMutableArray* stylableMapPoiFrameButton;
@property (nonatomic) IBOutletCollection(UILabel) NSMutableArray* stylableMapPoiFrameTitle;
@property (nonatomic) IBOutletCollection(UILabel) NSMutableArray* stylableMapPoiFrameAddress;

@end
