//
//  CTXPOI.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <CoreLocation/CoreLocation.h>
#import <CTXSDK/CLJastor.h>

@interface CTXPOI : CLJastor

#pragma mark - Properties
@property (nonatomic) NSNumber *lat;
@property (nonatomic) NSNumber *lon;
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *text;
@property (nonatomic) NSString *address;
@property (nonatomic) NSNumber *previewImageId;
@property (nonatomic) NSNumber *imageId;
@property (nonatomic) NSNumber *interactionId;

#pragma mark - Custom Properties
@property (nonatomic) CLLocationDistance distance;
@property (nonatomic) CLLocation *location;
- (CLLocationDistance)computeDistanceWithLocation:(CLLocation *)location;

@end
