//
//  CTXPOIActionInfoResponse.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <CTXSDK/C345Response.h>
#import <CTXSDK/CTXBaseActionInfoResponseBody.h>

#pragma mark - Class CTXPOIActionInfoResponseBody
@interface CTXPOIActionInfoResponseBody : CTXBaseActionInfoResponseBody

#pragma mark - Properties
@property (nonatomic) NSString  *header;
@property (nonatomic) NSString  *title;
@property (nonatomic) NSString  *subTitle;
@property (nonatomic) NSNumber  *previewImageId;
@property (nonatomic) NSNumber  *locationPINImageId;
@property (nonatomic) NSNumber  *userPINImageId;
@property (nonatomic) NSNumber  *logoImageId;
@property (nonatomic) NSString  *videoURL;
@property (nonatomic) NSArray   *pois;
@property (nonatomic) NSNumber  *logoImageIsHeader;

@end


#pragma mark - Class CTXPOIActionInfoResponse
@interface CTXPOIActionInfoResponse : C345Response

#pragma mark - Properties
@property (nonatomic) CTXPOIActionInfoResponseBody *body;

@end
