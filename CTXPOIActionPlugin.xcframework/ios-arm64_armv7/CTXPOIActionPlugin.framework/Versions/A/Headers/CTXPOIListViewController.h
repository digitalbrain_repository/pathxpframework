//
//  CTXPOIListViewController.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <UIKit/UIKit.h>
#import <CTXPOIActionPlugin/CTXPOIActionInfoResponse.h>
#import <CTXPOIActionPlugin/CTXPOIFullViewController.h>
#import <CTXPOIActionPlugin/CTXPOIActionStyle.h>

#define CTX_POI_LIST_VC_IDENTIFIER  @"CTXPOIListVCIdentifier"

@class CTXActionPlugin;

@interface CTXPOIListViewController : UIViewController <CTXPOIActionStylable>

#pragma mark - Properties
@property (nonatomic) id<CTXPOIDelegate> delegate;
@property (nonatomic) CTXPOIActionInfoResponseBody *poiInfo;
@property (nonatomic, weak) CTXActionPlugin * responsiblePlugin;
@property (nonatomic, weak) CTXPOIActionStyle * style;

@property (nonatomic) IBOutletCollection(UIView) NSMutableArray* stylableFrame;

@end
