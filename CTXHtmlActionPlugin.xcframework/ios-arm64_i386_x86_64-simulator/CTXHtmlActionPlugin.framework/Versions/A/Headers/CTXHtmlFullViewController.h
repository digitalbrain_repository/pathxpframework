//
//  CTXHtmlFullViewController.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <UIKit/UIKit.h>
#import <CTXHtmlActionPlugin/CTXHtmlActionInfoResponse.h>
#import <CTXSDK/CTXBaseActionViewController.h>
#import <WebKit/WebKit.h>

#define CTX_HTML_FULL_VIEWCONTROLLER_IDENTIFIER   @"CTXHtmlFullVCIdentifier"

@interface CTXHtmlFullViewController : CTXBaseActionViewController <WKNavigationDelegate>

#pragma mark - Properties
@property (nonatomic) CTXHtmlActionInfoResponseBody *htmlInfo;


@end
