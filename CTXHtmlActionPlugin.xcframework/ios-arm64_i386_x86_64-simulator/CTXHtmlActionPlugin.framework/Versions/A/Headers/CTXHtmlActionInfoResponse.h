//
//  CTXHtmlActionInfoResponse.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//
#import <UIKit/UIKit.h>
#import <CTXSDK/C345Response.h>
#import <CTXSDK/CTXBaseActionInfoResponseBody.h>

#pragma mark - Class CTXHtmlActionInfoResponseBody
@interface CTXHtmlActionInfoResponseBody : CTXBaseActionInfoResponseBody

#pragma mark - Properties
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *text;
@property (nonatomic) NSNumber *imageId;
@property (nonatomic) NSString *body;

#pragma mark - Custom Properties
//@property (nonatomic) UIImage *image;
@property (nonatomic, readonly) NSURL *htmlUrl;

@end

#pragma mark - Class CTXHtmlActionInfoResponse
@interface CTXHtmlActionInfoResponse : C345Response

#pragma mark - Properties
@property (nonatomic) CTXHtmlActionInfoResponseBody *body;

@end
