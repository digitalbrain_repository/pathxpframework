//
//  CTXDownloadActionPlugin.h
//  ContextSDK
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <CTXSDK/CTXActionPlugin.h>
#import <CTXDownloadActionPlugin/CTXDownloadActionInfoResponse.h>

@protocol CTXDownloadActionPluginDelegate <NSObject>
- (void)didSelectItemToDownload:(CTXDownloadActionInfoResponseBody *)item completion:(void(^)(BOOL success))completion ;

@end

@interface CTXDownloadActionPlugin : CTXActionPlugin
@property (nonatomic, weak) id <CTXDownloadActionPluginDelegate> downloadActionDelegate;
@end
