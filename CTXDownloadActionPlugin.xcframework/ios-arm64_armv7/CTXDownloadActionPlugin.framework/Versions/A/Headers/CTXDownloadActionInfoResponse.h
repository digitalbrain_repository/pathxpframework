//
//  CTXDownloadActionInfoResponse.h
//  ContextSDK
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <CTXSDK/C345Response.h>
#import <CTXSDK/CTXBaseActionInfoResponseBody.h>

@interface CTXDownloadActionButtonUrl :  CLJastor
@property (nonatomic) NSString *type;
@property (nonatomic) NSString *label;
@property (nonatomic) NSString *filename;
@property (nonatomic) NSString *fileext;
@property (nonatomic) NSString *origType;
@property (nonatomic) NSString *url;
@property (nonatomic) NSInteger position;
@property (nonatomic) NSString *color;
@property (nonatomic) NSNumber *saveInApp;
@property (nonatomic) NSNumber *saveInGallery;
@property (nonatomic) NSNumber *autoDownload;
@end

@interface CTXDownloadActionInfoResponseBody : CTXBaseActionInfoResponseBody
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *text;
@property (nonatomic) NSNumber *imageId;
@property (nonatomic) NSArray *buttons;

@end

@interface CTXDownloadActionInfoResponse : C345Response
@property (nonatomic) CTXDownloadActionInfoResponseBody *body;
@end
