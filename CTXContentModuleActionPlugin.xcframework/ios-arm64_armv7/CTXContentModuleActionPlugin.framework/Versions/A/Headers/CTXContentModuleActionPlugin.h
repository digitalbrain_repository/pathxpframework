//
//  CTXContentModuleActionPlugin.h
//  DefaultPlugins
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 3249741e5a7bdf0e039ce875227aae8b45d03c1c
//

#import <CTXSDK/CTXActionPlugin.h>

@class CTXContentModuleFullViewController;
@class UIColor;

@protocol CTXContentModuleDelegate <NSObject>

@optional

- (UIColor*)contentModule:(CTXContentModuleFullViewController*)contentModule backgroundColorForContentAtIndex:(NSInteger)index;
- (UIColor*)contentModule:(CTXContentModuleFullViewController*)contentModule borderColorForContentAtIndex:(NSInteger)index;
- (UIColor*)contentModule:(CTXContentModuleFullViewController*)contentModule textColorForContentAtIndex:(NSInteger)index;
- (BOOL)contentModule:(CTXContentModuleFullViewController*)contentModule squeezableForContentAtIndex:(NSInteger)index;

@end


@interface CTXContentModuleActionPlugin : CTXActionPlugin

@property (nonatomic, weak) id<CTXContentModuleDelegate> contentModuleDelegate;

@end
