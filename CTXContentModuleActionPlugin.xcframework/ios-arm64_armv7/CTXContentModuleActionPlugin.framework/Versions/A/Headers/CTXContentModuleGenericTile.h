//
//  CTXContentModuleGenericTile.h
//  DefaultPlugins
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 3249741e5a7bdf0e039ce875227aae8b45d03c1c
//

#import <CTXSDK/CTXGenericTileViewController.h>

@interface CTXContentModuleGenericTile : CTXGenericTileViewController

@end
