//
//  CTXCacheContentModuleActionsResponse.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 3249741e5a7bdf0e039ce875227aae8b45d03c1c
//

#import <CTXSDK/C345Response.h>
#import <CTXSDK/CTXCacheBaseActionsResponseBody.h>

@interface CTXCacheContentModuleActionsResponseBody : CTXCacheBaseActionsResponseBody

#pragma mark - Properties
@property (nonatomic, strong) NSArray *contentModuleActions;

@end

@interface CTXCacheContentModuleActionsResponse : C345Response

#pragma mark - Properties
@property (nonatomic) CTXCacheContentModuleActionsResponseBody *body;

@end
