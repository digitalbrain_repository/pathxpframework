//
//  CTXLinkActionInfoResponse.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <CTXSDK/C345Response.h>

#import <CTXSDK/CTXBaseActionInfoResponseBody.h>

#pragma mark - Class CTXLinkActionInfoResponseBody
@interface CTXLinkActionInfoResponseBody : CTXBaseActionInfoResponseBody

#pragma mark - Properties
@property (nonatomic) NSString  *title;
@property (nonatomic) NSString  *subTitle;
@property (nonatomic) NSNumber  *previewImageId;
@property (nonatomic) NSNumber  *linkActionId;
@property (nonatomic) NSNumber  *linkEventId;

@end


#pragma mark - Class CTXLinkActionInfoResponse
@interface CTXLinkActionInfoResponse : C345Response

#pragma mark - Properties
@property (nonatomic) CTXLinkActionInfoResponseBody *body;

@end
