//
//  CTXCacheLandingActionsResponse.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <CTXSDK/C345Response.h>
#import <CTXSDK/CTXCacheBaseActionsResponseBody.h>

@interface CTXCacheLandingActionsResponseBody : CTXCacheBaseActionsResponseBody

#pragma mark - Properties
@property (nonatomic, strong) NSArray *landingActions;

@end

@interface CTXCacheLandingActionsResponse : C345Response

#pragma mark - Properties
@property (nonatomic) CTXCacheLandingActionsResponseBody *body;

@end
