//
//  CTXLandingViewController.h
//  ContextSDK
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
#import <CTXLandingActionPlugin/CTXLandingActionInfoResponse.h>
#import <CTXSDK/CTXBaseActionViewController.h>

#define CTX_LANDING_FULL_VIEWCONTROLLER_IDENTIFIER   @"CTXLandingFullVCIdentifier"

@interface CTXLandingFullViewController : CTXBaseActionViewController <WKNavigationDelegate>

#pragma mark - Properties
@property (nonatomic) CTXLandingActionInfoResponseBody *landingInfo;
@property (weak, nonatomic) IBOutlet UIView *webViewContainer;
@property (nonatomic, weak) IBOutlet UIView *navBar;
@property (nonatomic, weak) IBOutlet UILabel *titlelbl;
@property (nonatomic) IBOutlet NSLayoutConstraint *navBarHeight;

@end
