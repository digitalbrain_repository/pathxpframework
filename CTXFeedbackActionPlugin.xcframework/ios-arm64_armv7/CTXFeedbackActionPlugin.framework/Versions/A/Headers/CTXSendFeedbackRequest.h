//
//  CTXSendFeedbackRequest.h
//  ContextSDK
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <CTXSDK/C345Request.h>

@interface CTXSendFeedbackRequestBody : CTXRequestBody

#pragma mark - Properties
@property (nonatomic) NSString *response;

@end

@interface CTXSendFeedbackRequest : C345Request <CTXRequestDelegate>

#pragma mark - Inizializer
- (instancetype)initWithTraceID:(NSString*)traceID;

@end
