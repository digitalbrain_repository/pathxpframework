//
//  CTXFeedbackActionInfoResponse.h
//  ContextSDK
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <CTXSDK/C345Response.h>
#import <CTXSDK/CTXBaseActionInfoResponseBody.h>

#pragma mark - Class CTXFeedbackActionInfoResponseBody
@interface CTXFeedbackActionInfoResponseBody : CTXBaseActionInfoResponseBody

#pragma mark - Properties
@property (nonatomic, copy) NSString *question;
@property (nonatomic, strong) NSArray *acceptedResponses;

@end

#pragma mark - Class CTXFeedbackActionInfoResponse
@interface CTXFeedbackActionInfoResponse : C345Response


#pragma mark - Properties
@property (nonatomic) CTXFeedbackActionInfoResponseBody *body;

@end
