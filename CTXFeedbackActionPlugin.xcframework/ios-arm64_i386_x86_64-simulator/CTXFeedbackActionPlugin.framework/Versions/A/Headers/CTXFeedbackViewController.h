//
//  CTXFeedbackViewController.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <UIKit/UIKit.h>
#import <CTXFeedbackActionPlugin/CTXFeedbackActionInfoResponse.h>
#import <CTXSDK/CTXBaseActionViewController.h>

#define CTX_FEEDBACK_VIEWCONTROLLER_IDENTIFIER   @"CTXFeedbackVCIdentifier"

@interface CTXFeedbackViewController : CTXBaseActionViewController

#pragma mark - Properties
@property (nonatomic) CTXFeedbackActionInfoResponseBody *feedbackInfo;

@end
