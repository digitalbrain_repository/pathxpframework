//
//  CTXLandingPushInfoResponse.h
//  ContextSDK
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <CTXSDK/C345Response.h>
#import <CTXSDK/CTXBaseActionInfoResponseBody.h>

#pragma mark - Class CTXLandingActionInfoResponseBody
@interface CTXLandingActionInfoResponseBody : CTXBaseActionInfoResponseBody

#pragma mark - Properties
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *text;
@property (nonatomic) NSString *url;
@property (nonatomic) NSNumber *imageId;
@property (nonatomic) NSNumber *enableCamera;

#pragma mark - Custom Properties
//@property (nonatomic) UIImage *image;

@end

#pragma mark - Class CTXLandingActionInfoResponse
@interface CTXLandingActionInfoResponse : C345Response

#pragma mark - Properties
@property (nonatomic) CTXLandingActionInfoResponseBody *body;

@end
