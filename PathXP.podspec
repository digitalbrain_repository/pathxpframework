Pod::Spec.new do |s|
    s.name              = 'PathXP'
    s.version           = '0.12.5.0.6'
    s.summary           = 'The PathXP Framework'
    s.homepage          = 'http://example.com'

    s.author            = { 'DigitalBrain' => 'digitalbrain@hotmail.it' }


    s.platform          = :ios
    s.source = {:git => "https://digitalbrain_repository@bitbucket.org/digitalbrain_repository/pathxpframework.git"}

    s.ios.deployment_target = '9.0'
    s.ios.vendored_frameworks = '*.xcframework'
    s.resources = ['*.bundle']
end
