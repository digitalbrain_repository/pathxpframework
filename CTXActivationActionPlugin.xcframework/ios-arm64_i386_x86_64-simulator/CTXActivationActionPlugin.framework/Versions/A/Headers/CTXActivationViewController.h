//
//  CTXActivationViewController.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <UIKit/UIKit.h>
#import <CTXActivationActionPlugin/CTXActivationActionInfoResponse.h>
#import <CTXSDK/CTXBaseActionViewController.h>

@interface CTXActivationViewController : CTXBaseActionViewController
#define CTX_ACTIVATION_VIEW_CONTROLLER_IDENTIFIER   @"CTXActivationVCIdentifier"

#pragma mark - OUTLET
@property (nonatomic,weak) IBOutlet UIImageView *logoImgView;
@property (nonatomic,weak) IBOutlet UIImageView *heartImgView;
@property (nonatomic,weak) IBOutlet UILabel     *claimLbl;

@property (nonatomic,strong) CTXActivationActionInfoResponseBody *activationActionInfo;

-(IBAction)buttonJoinAction:(id)sender;
@end