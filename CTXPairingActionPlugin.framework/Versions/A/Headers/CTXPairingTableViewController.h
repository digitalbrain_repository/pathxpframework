//
//  CTXPairingTableViewController.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <UIKit/UIKit.h>
#import <CTXPairingActionPlugin/CTXPairingActionInfoResponse.h>
#import <CTXPairingActionPlugin/CTXPairingTableViewCell.h>
#import <CTXSDK/CTXBaseActionViewController.h>

#define CTX_PAIRING_TABLE_VIEW_CONTROLLER_IDENTIFIER   @"CTXPairingTableVCIdentifier"

@interface CTXPairingTableViewController : CTXBaseActionViewController <UITableViewDelegate, UITableViewDataSource, CTXPairingTableViewCellDelegate>

#pragma mark - Outlets
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *switchMarketing;
@property (weak, nonatomic) IBOutlet UIButton *buttonContinue;
@property (weak, nonatomic) IBOutlet UIButton *switchProfiling;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *buttonPrivacy;

#pragma mark - Actions
- (IBAction)switchMarketingAction:(id)sender;
- (IBAction)buttonContinueAction:(id)sender;
- (IBAction)buttonCloseAction:(id)sender;

#pragma mark - Properties
@property (nonatomic) CTXPairingActionInfoResponseBody *pairingInfo;


@end
