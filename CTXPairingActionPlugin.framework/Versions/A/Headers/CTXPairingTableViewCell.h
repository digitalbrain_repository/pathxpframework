//
//  CTXPairingTableViewCell.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <UIKit/UIKit.h>

#define CTX_PAIRING_TABLE_VIEWCELL_INDENTIFIER  @"CTXPairingCellViewIdentifier"

@class CTXPairingTableViewCell;

@protocol CTXPairingTableViewCellDelegate <NSObject>

- (void)editedCell:(CTXPairingTableViewCell*)cell atIndexPath:(NSIndexPath*)indexPath;

@end

@interface CTXPairingTableViewCell : UITableViewCell <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet id<CTXPairingTableViewCellDelegate>delegate;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (nonatomic) NSIndexPath *indexPath;

@end
