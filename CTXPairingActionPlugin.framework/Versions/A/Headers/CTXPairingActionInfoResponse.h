//
//  CTXPairingActionInfoResponse.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//
#import <UIKit/UIKit.h>
#import <CTXSDK/C345Response.h>
#import <CTXSDK/CTXBaseActionInfoResponseBody.h>
#import <CTXSDK/CTXBeacon.h>

#pragma mark - Class CTXPairingActionInfoResponseBody
@interface CTXPairingActionInfoResponseBody : CTXBaseActionInfoResponseBody

#pragma mark - Properties
@property (nonatomic) NSNumber *imageId;

#pragma mark - Custom Properties
@property (nonatomic) CTXBeacon *beaconInfo;
//@property (nonatomic) UIImage *image;

@end

#pragma mark - Class CTXPairingActionInfoResponse
@interface CTXPairingActionInfoResponse : C345Response

#pragma mark - Properties
@property (nonatomic) CTXPairingActionInfoResponseBody *body;

@end
