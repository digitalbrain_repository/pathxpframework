//
//  CTXViedoFullScreenViewController.h
//  CTXContentModuleActionPlugin
//
//  Created by Massimiliano on 17/05/18.
//  Copyright © 2018 com.mxm345. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVKit/AVKit.h>

@interface CTXViedoFullScreenViewController : AVPlayerViewController
@property (nonatomic,copy) NSString *url;
+ (CTXViedoFullScreenViewController *)createWithURL:(NSString *)url;
@end
