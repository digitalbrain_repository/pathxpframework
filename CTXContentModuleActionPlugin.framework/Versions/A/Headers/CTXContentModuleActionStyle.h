//
//  CTXContentModuleActionStyle.h
//  ContentModuleActionPlugin
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <CTXSDK/CTXBaseActionStyle.h>

typedef NS_ENUM(NSInteger, CTXContentModuleItemLayout) {
    CTXContentModuleItemLayoutOverlayText       = 0,
    CTXContentModuleItemLayoutFixedSizeText     = 1,
    CTXContentModuleItemLayoutDinamicSize       = 2,
};

@protocol CTXContentModuleActionStylable <CTXBaseActionStylable>

/*@property (nonatomic) IBOutletCollection(UIView) NSMutableArray * stylableDragBackground;
@property (nonatomic) IBOutletCollection(UIImageView) NSMutableArray * stylableDragBackgroundImage;
@property (nonatomic) IBOutletCollection(UIImageView) NSMutableArray * stylableDragIcon;
@property (nonatomic) IBOutletCollection(UIImageView) NSMutableArray * stylableDragShadow;
@property (nonatomic) IBOutletCollection(UIView) NSMutableArray * stylableDragSeparator;
@property (nonatomic) IBOutletCollection(UILabel) NSMutableArray * stylableDragDescription;
*/
@end

@interface CTXContentModuleActionStyle : CTXBaseActionStyle

@property (nonatomic) CTXImageResource * dragIcon;
@property (nonatomic) CTXImageResource * dragBackgroundImage;
@property (nonatomic) UIColor * dragBackgroundColor;
@property (nonatomic) UIColor * dragSeparatorColor;
@property (nonatomic) UIColor * dragDescriptionColor;
@property (nonatomic) CTXFontResource * dragDescriptionFont;

@property (nonatomic) UIColor * dragShadowBgColor;
@property (nonatomic) CTXImageResource * dragShadowImage;
@property (nonatomic) CTXContentModuleItemLayout itemLayout;

-(void)applyTo:(id<CTXContentModuleActionStylable>)stylable;


@end
