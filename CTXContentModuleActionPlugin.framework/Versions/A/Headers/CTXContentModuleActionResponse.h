//
//  CTXContentModuleActionResponse.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//
#import <UIKit/UIKit.h>
#import <CTXSDK/C345Response.h>
#import <CTXSDK/CTXBaseActionInfoResponseBody.h>
#import <CTXSDK/C345Action.h>

#pragma mark - Class CTXContentModuleActionResponseBody
@interface CTXContentModuleActionResponseBody : CTXBaseActionInfoResponseBody

#pragma mark - Properties
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *text;
@property (nonatomic) NSNumber *imageId;
@property (nonatomic) NSArray *media;
@property (nonatomic) NSString *dotsColor;
@property (nonatomic) NSString *dotsSelectedColor;
@property (nonatomic) NSString *dotsPosition;
@property (nonatomic) NSString *descriptionColor;
@property (nonatomic) NSString *commentColor;
@property (nonatomic) NSString *bgColor;
@property (nonatomic) NSString *titleColor;

#pragma mark - Custom Properties
//@property (nonatomic) UIImage *image;

@end

#pragma mark - Class CTXContentModuleActionResponse
@interface CTXContentModuleActionResponse : C345Response

#pragma mark - Properties
@property (nonatomic) CTXContentModuleActionResponseBody *body;

@end
