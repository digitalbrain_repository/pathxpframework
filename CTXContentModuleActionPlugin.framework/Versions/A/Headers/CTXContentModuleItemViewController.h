//
//  CTXContentModuleItemViewController.h
//  ContentModuleActionPlugin
//
//  Created by Massimiliano on 18/04/17.
//  Copyright © 2017 com.mxm345. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CTXContentModuleActionPlugin/CTXContentModuleActionStyle.h>
#import <CTXSDK/CTXMedia.h>
#import <CTXSDK/CLAdvancedAttributedLabel.h>


@interface CTXScrollableContentModuleContentView : UIView
@property (nonatomic) BOOL scrollInRect;
@property (nonatomic, weak) IBOutlet CLAdvancedAttributedLabel *titleLbl;
@property (nonatomic, weak) IBOutlet CLAdvancedAttributedLabel *descriptionLbl;
@property (nonatomic) IBOutlet NSLayoutConstraint *constraint;
@property (nonatomic, strong) UIFont *titleFont;
@property (nonatomic, strong) UIFont *descrFont;
@end


@protocol CTXContentModuleItemDelegate;

@interface CTXContentModuleItemViewController : UIViewController

@property (nonatomic, weak) CTXCallToAction *globalCallToAction;
@property (nonatomic) CTXContentModuleItemLayout layout;
@property (nonatomic, weak) id <CTXContentModuleItemDelegate> delegate;
@property (nonatomic) NSInteger index;
@property (nonatomic, weak) IBOutlet CTXScrollableContentModuleContentView *textContainer;
@property (nonatomic) CGFloat textBoxHeight;
@property (nonatomic, strong) UIFont *titleFont;
@property (nonatomic, strong) UIFont *descrFont;
- (void)updateWitMedia:(CTXMedia *)media;
@end


@protocol CTXContentModuleItemDelegate <NSObject>
- (void)contentModuleItem: (CTXContentModuleItemViewController *)item tappedOnCallToAction:(CTXCallToAction *)callToAction;
- (void)contentModuleItemNeedCustonLayout: (CTXContentModuleItemViewController *) item;

@end
