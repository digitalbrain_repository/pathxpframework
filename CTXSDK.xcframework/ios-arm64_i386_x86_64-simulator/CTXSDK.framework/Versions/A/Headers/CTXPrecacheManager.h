//
//  CTXPrecacheManager.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>

@interface CTXPrecacheManager : NSObject

/**
 *  Try to inject cache files from the CTXPrecacheBundle, if present.
 *  Precaching is disabled on simulator.
 *  This method can be called safely more than once, as it doesn't overwrite files if they already exist.
 *
 */
+ (void) exhumeFromBundle: (void(^)(void)) completionHandler;


@end
