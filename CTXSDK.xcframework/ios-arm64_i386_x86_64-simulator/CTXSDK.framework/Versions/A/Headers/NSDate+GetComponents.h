//
//  NSDate+GetComponents.h
//  Friwix
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>

@interface NSDate (GetComponents)

-(long)year;
-(long)month;
-(long)day;
-(long)hour;

- (NSDate*) dateByAddingYears: (int) numberOfYears;

@end
