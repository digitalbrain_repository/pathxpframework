//
//  CTXEvent.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <CTXSDK/CLJastor.h>

@interface CTXEvent : CLJastor

#pragma mark - Properties
@property (copy, nonatomic) NSString *storeTitle;
@property (copy, nonatomic) NSString *storeId;
@property (copy, nonatomic) NSString *storeAddress;
@property (copy, nonatomic) NSString *storePhone;
@property (copy, nonatomic) NSNumber *latitude;
@property (copy, nonatomic) NSNumber *longitude;
@property (copy, nonatomic) NSString *type;
@property (copy, nonatomic) NSString *city;
@property (copy, nonatomic) NSString *date;
@property (copy, nonatomic) NSString *dateText;
@property (copy, nonatomic) NSString *titleEvent;
@property (copy, nonatomic) NSString *descript;
@property (copy, nonatomic) NSString *image;
@property (copy, nonatomic) NSString *eanCode;

@end