//
//  CTXNotificationActionInfoResponse.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <CTXSDK/C345Response.h>
#import <CTXSDK/CTXBaseActionInfoResponseBody.h>

#pragma mark - Class CTXNotificationActionInfoResponseBody

@interface CTXNotificationActionInfoResponseBody : CTXBaseActionInfoResponseBody

@property (nonatomic) NSString * notificationText;
@property (nonatomic) NSNumber * imageId;

@property (nonatomic) NSString * tag;


#pragma mark - Custom properties

@property (nonatomic) NSDate * dateReceived;

@end

#pragma mark - Class CTXNotificationActionInfoResponse

@interface CTXNotificationActionInfoResponse : C345Response

@property (nonatomic) CTXNotificationActionInfoResponseBody *body;

@end
