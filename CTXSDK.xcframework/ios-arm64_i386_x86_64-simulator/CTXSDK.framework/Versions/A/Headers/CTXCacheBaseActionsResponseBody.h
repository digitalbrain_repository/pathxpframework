//
//  CTXCacheBaseActionsResponseBody.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <CTXSDK/C345Response.h>

@interface CTXCacheBaseActionsResponseBody : CTXResponseBody

@property (nonatomic, readonly) NSArray* elements;

@end
