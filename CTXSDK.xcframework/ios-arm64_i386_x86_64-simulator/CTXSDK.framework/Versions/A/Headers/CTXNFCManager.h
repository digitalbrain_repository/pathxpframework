//
//  CTXNFCManager.h
//  CTXSDK
//
//  Created by Massimiliano on 15/05/18.
//  Copyright © 2018 01tribe. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreNFC;

@interface CTXNFCManager : NSObject <NFCNDEFReaderSessionDelegate>
+ (instancetype)sharedInstance;
- (void)checkNFC:(NSString *)nfcValue;
- (void)startSession;
@end
