//
//  CLCacheManager.h
//  CoreLibrary
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>


@interface CLCacheManager : NSObject

#pragma mark - SharedInstance
+ (instancetype)manager;

- (id) cachedObjectForOperation:(NSURLSessionDataTask *)operation;
- (void) saveCachedObject:(id)object forOperation:(NSURLSessionDataTask *)operation;

@end
