//
//  ContextLoginRequest.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>
#import <CTXSDK/C345Request.h>

#pragma mark - Defines
typedef NS_ENUM(NSInteger, ContextLoginType)
{
    ContextLoginBuiltInType = 0,    // login nativa su piattaforma Context
    ContextLoginSocialType,         // login Social
    ContextLoginCustomerType,       // login Customer
    ContextLoginFakeType,           // login Fake
    ContextLoginTypeCounts
};

typedef NS_ENUM(NSInteger, ContextSocialType)
{
    ContextSocialNONEType = 0,
    ContextSocialFacebookType,  // login Social Facebook
    ContextSocialTwitterType,       // login Twitter Facebook
    ContextSocialPaypalType,
    ContextSocialTypeCounts
};

// loginTypes
#define CTX_LOGIN_BUILTIN           @"builtIn"
#define CTX_LOGIN_SOCIAL            @"social"
#define CTX_LOGIN_CUSTOMER          @"customer"
#define CTX_LOGIN_FAKE              @"fake"

// socialTypes
#define CTX_LOGIN_FACEBOOK          @"facebook"
#define CTX_LOGIN_TWITTER           @"twitter"
#define CTX_LOGIN_PAYPAL            @"paypal"
#pragma mark - Class C345Login
@interface C345Login : CLJastor

@property (nonatomic) NSString *username;
@property (nonatomic) NSString *password;
@property (nonatomic) NSString *socialType;
@property (nonatomic) NSString *accessToken;
@property (nonatomic) NSString *additionalInfo1;
@property (nonatomic) NSString *additionalInfo2;
@property (nonatomic) NSString *userUDID;

@property (nonatomic) NSString *socialName;
@property (nonatomic) NSString *socialToken;


@end

#pragma mark - Class CTXLoginRequestBody
@interface CTXLoginRequestBody : CTXRequestBody

@property (nonatomic) NSString *loginType;
@property (nonatomic) C345Login *builtIn;
@property (nonatomic) C345Login *social;
@property (nonatomic) C345Login *customer;
@property (nonatomic) C345Login *fake;
@property (nonatomic) NSString* statisticDeviceId;

@end

#pragma mark - Class CTXLoginRequest
@interface CTXLoginRequest : C345Request <CTXRequestDelegate>

-(instancetype)initWithLoginType:(ContextLoginType)loginType socialType:(ContextSocialType)socialType;

#pragma mark - Functions
+ (NSString*)loginTypeStringWithEnum:(ContextLoginType)loginType;
+ (NSString*)socialTypeStringWithEnum:(ContextSocialType)socialType;
@end
