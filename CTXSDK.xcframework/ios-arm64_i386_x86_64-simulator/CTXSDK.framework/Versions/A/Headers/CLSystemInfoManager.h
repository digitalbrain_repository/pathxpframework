//
//  SystemInfo.h
//  CoreLibrary
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>
#import <CTXSDK/CLMessagesDelegate.h>

@interface CLSystemInfoManager : NSObject

#pragma mark - SharedInstance
+ (instancetype)manager;

#pragma mark - Delegates
@property (nonatomic) id<CLMessagesDelegate> messagesDelegate;

#pragma mark - Properties
@property (nonatomic) NSString *genericErrorMsg;


@end
