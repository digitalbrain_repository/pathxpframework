//
//  CTXPluginStyle.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>

#import <CTXSDK/CTXImageResource.h>
#import <CTXSDK/CTXFontResource.h>


@protocol CTXPluginStylable <NSObject>

@end

@interface CTXPluginStyle : NSObject

- (void) applyTo: (id<CTXPluginStylable>) stylable;

@end
