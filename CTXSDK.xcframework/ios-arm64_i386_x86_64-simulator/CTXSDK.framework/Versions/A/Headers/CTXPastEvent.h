//
//  CTXPastEvent.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <CTXSDK/CLJastor.h>

@class CTXInteraction, C345Action, CTXBaseActionInfoResponseBody, CTXInteractionInfoResponseBody;

@interface CTXPastEvent : CLJastor

@property (nonatomic) CTXInteraction * interaction;
@property (nonatomic) C345Action * action;
@property (nonatomic) CTXBaseActionInfoResponseBody * actionInfo;
@property (nonatomic) CTXInteractionInfoResponseBody * interactionInfo;

@end
