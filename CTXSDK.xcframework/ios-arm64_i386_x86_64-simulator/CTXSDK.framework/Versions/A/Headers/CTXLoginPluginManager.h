//
//  CTXLoginPluginManager.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <CTXSDK/CTXLoginDelegate.h>
#import <CTXSDK/CTXLoginManager.h>
#import <CTXSDK/CTXLoginPluginResponse.h>

@interface CTXLoginPluginManager : NSObject <CTXLoginDelegate>

#pragma mark - SharedInstance
+ (instancetype)manager;

#pragma mark - Functions
- (void)loginWithUsername:(NSString*)username
              andPassword:(NSString*)password
                  success:(CTXLoginPluginSuccessResponseHandler)success
                  failure:(CTXFailedResponseHandler)failure;

- (void)loginSocialWithType:(ContextSocialType)socialType
                      token:(NSString*)token
                    success:(CTXLoginPluginSuccessResponseHandler)success
                    failure:(CTXFailedResponseHandler)failure;

- (void)refreshLoginWithCompletion:(CTXLoginPluginSuccessResponseHandler)success
                        failure:(CTXFailedResponseHandler)failure;

- (void)refreshLoginWithRefreshTokenWithCompletion:(CTXLoginPluginSuccessResponseHandler)success
                                           failure:(CTXFailedResponseHandler)failure;

- (void)logoutWithSuccess:(CTXLogoutSuccessResponseHandler)success
                  failure:(CTXFailedResponseHandler)failure;

- (void) resetLoginState;

@end
