//
//  CTXPassbookResponse.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <CTXSDK/C345Response.h>
#import <PassKit/PassKit.h>

#pragma mark - Class CTXPassbookResponseBody
@interface CTXPassbookResponseBody : CTXResponseBody

#pragma mark - Properties
@property (nonatomic) NSString *pkpass;

@end

#pragma mark - Class CTXPassbookResponse
@interface CTXPassbookResponse : C345Response

#pragma mark - Properties
@property (nonatomic) CTXPassbookResponseBody *body;
@property (nonatomic) PKPass *passbook;

@end
