//
//  CTXActionHeader.h
//  CTXSDK
//
//  Created by Massimiliano on 15/12/17.
//  Copyright © 2017 01tribe. All rights reserved.
//

#import <CTXSDK/CLJastor.h>

@interface CTXActionHeaderCloseButton: CLJastor
@property (nonatomic) NSNumber *visible;
@property (nonatomic) NSString *position;
@property (nonatomic) NSString *backgroundColor;
@property (nonatomic) NSString *iconColor;
@end

@interface CTXActionHeader : CLJastor
@property (nonatomic) NSNumber *visible;
@property (nonatomic) NSString *backgroundColor;
@property (nonatomic) NSString *textColor;
@property (nonatomic) NSString *title;
@property (nonatomic) CTXActionHeaderCloseButton *close;

@end


