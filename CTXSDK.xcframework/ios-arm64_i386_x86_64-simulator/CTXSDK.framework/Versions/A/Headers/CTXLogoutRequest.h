//
//  CTXLogoutRequest.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <CTXSDK/C345Request.h>

#pragma mark - Class CTXLogoutRequest
@interface CTXLogoutRequest : C345Request <CTXRequestDelegate>

@end
