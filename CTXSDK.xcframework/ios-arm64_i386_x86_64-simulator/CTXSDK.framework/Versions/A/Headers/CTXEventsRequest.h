//
//  CTXEventsRequest.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <CTXSDK/C345Request.h>

#pragma mark - Defines

#define CTX_EVENTS_ACTION        @"actionGetEvents"

#pragma mark - Class CTXEventsRequestBody
@interface CTXEventsRequestBody : CTXRequestBody

@property (nonatomic) NSNumber *limit;
@property (nonatomic) NSNumber *lat;
@property (nonatomic) NSNumber *lon;
@property (nonatomic) NSNumber *nelng;
@property (nonatomic) NSNumber *nelat;
@property (nonatomic) NSNumber *swlng;
@property (nonatomic) NSNumber *swlat;

@end

@interface CTXEventsRequest : C345Request <CTXRequestDelegate>

#pragma mark - Inizializer
- (instancetype)initWithWorkflowName:(NSString *)workflowName;

@end