//
//  CTXGeoRegion.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <CTXSDK/CLJastor.h>

@interface CTXGeoRegion : CLJastor

#pragma mark - Properties
@property (copy, nonatomic) NSNumber *lat;
@property (copy, nonatomic) NSNumber *lon;
@property (copy, nonatomic) NSNumber *radius;

@end
