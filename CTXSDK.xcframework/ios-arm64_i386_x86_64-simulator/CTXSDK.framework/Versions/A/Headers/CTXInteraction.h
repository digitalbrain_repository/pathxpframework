//
//  CTXInteraction.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>
#import <CTXSDK/CTXBeacon.h>
#import <CTXSDK/CTXTrigger.h>
#import <CTXSDK/CTXGeoRegion.h>
#import <CTXSDK/CLBaseModel.h>
#import <CTXSDK/CTXInteractionInfoResponse.h>

#pragma mark - Defines
static NSString *const kInteractionKey       = @"interaction";
static NSString *const kPauseReasonClosedByUser = @"closedByUser";
static NSString *const kPauseReasonNotificationSent = @"notificationSent";

@interface CTXInteraction : CLJastor

- (instancetype)initWithTrigger:(CTXTrigger*)trigger;

#pragma mark - Readonly properties
@property (nonatomic, readonly) NSNumber *interactionId;
@property (nonatomic, readonly) CTXTrigger *trigger;
@property (nonatomic) BOOL shouldCacheActions;

#pragma mark - Trigger custom info
@property (nonatomic) CTXBeacon *beaconInfo;
@property (nonatomic) CTXGeoRegion *geoRegionInfo;
@property (nonatomic, assign) BOOL mightBePaused;

@property (nonatomic, getter=isDisplayed) BOOL displayed;

- (void) pauseIfNeededForReason: (NSString*) reason;
- (BOOL) isPausedForReason: (NSString*) reason;
- (void) removeInteractionFromKeepClosedList;
- (BOOL) isInKeepClosedList;

@end
