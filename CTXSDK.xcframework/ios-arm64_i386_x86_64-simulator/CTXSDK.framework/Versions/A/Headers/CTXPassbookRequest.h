//
//  CTXPassbookRequest.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//
#import <Foundation/Foundation.h>
#import <CTXSDK/C345Request.h>

#pragma mark - Class CTXPassbookSecondaryField
@interface CTXPassbookSecondaryField : CLJastor

@property (nonatomic) NSString *label;
@property (nonatomic) NSString *value;

@end

#pragma mark - Class CTXPassbookRequestBody
@interface CTXPassbookRequestBody : CTXRequestBody

#pragma mark - Properties
@property (nonatomic) NSString *barcodeContent;
@property (nonatomic) NSString *passTypeId;
@property (nonatomic) NSString *serialNumber;
@property (nonatomic) NSArray *secondaryFields;

@end

@interface CTXPassbookRequest : C345Request <CTXRequestDelegate>

@property (nonatomic) NSString *traceID;
/*
#pragma mark - Inizializer
- (instancetype)initWithTraceID:(NSString*)traceID;
*/
@end
