//
//  ContextMessagesManager.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CTXSDK/CLMessagesDelegate.h>
#import <CTXSDK/CoreLibrary.h>

#pragma mark - Defines

// *** MESSAGES TAGS ***
#define GENERIC_ERROR_MSG                       @"GENERIC_ERROR_MSG"
#define PUSH_NOTIFICATIONS_DISABLED_WARNING     @"PUSH_NOTIFICATIONS_DISABLED_WARNING"
#define PUSH_NOTIFICATIONS_SERVICE_ERROR        @"PUSH_NOTIFICATIONS_SERVICE_ERROR"
#define LOGIN_ERROR_MSG                         @"LOGIN_ERROR_MSG"
#define LOCALIZATION_DISABLE_WARNING            @"LOCALIZATION_DISABLE_WARNING"
#define FEEDBACK_SERVICE_ERROR                  @"FEEDBACK_SERVICE_ERROR"
#define MAIL_VALIDATION_ERROR                   @"MAIL_VALIDATION_ERROR"

@interface CTXMessagesManager : NSObject <CLMessagesDelegate, UIAlertViewDelegate>

#pragma mark - SharedInstance
+ (instancetype)manager;

#pragma mark - Functions
- (NSString*)getMessageByTag:(NSString*)tag;

- (void)showAlertWithTitle:(NSString*)title andMessage:(NSString *)message;
- (void)showAlertWithTitle:(NSString*)title andTag:(NSString *)messageTag;

- (void)showErrorWithTitle:(NSString*)title andMessage:(NSString *)message;
- (void)showErrorWithTitle:(NSString*)title andTag:(NSString *)messageTag;



/*
- (void)showAlertWithTitle:(NSString*)title
                andMessage:(NSString *)message
           buttonOKHandler:(VoidBlock)okHandler
       buttonCancelHandler:(VoidBlock)cancelHandler;
*/
@end
