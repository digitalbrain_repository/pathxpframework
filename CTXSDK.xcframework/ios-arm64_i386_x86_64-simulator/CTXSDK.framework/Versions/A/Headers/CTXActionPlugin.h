//
//  CTXActionPlugin.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <CTXSDK/CTXPlugin.h>

@class CTXDashboardTileBaseViewController;
@class CTXBaseActionViewController;
@class CTXDashboardBlock;
@class CTXBaseActionInfoResponseBody;
@class CTXActionInfoRequest;
@class CTXInteraction;
@class CTXCheckCacheResponseBody;
@class CTXPluginStyle;
@class CTXGenericTileStyle;

@interface CTXActionPlugin : CTXPlugin

# pragma mark - Backend parameters

// direct call
@property (nonatomic, readonly) NSString * workflowName;
@property (nonatomic, readonly) NSString * requestName;
@property (nonatomic, readonly) NSString * actionType;
@property (nonatomic, readonly) Class responseClass;
@property (nonatomic, readonly) NSString *api_version;
// batched cache
@property (nonatomic, readonly) NSString * checkCacheCountKey;
@property (nonatomic, readonly) NSString * checkCacheLastUpdateKey;
@property (nonatomic, readonly) NSString * cacheRequestName;
@property (nonatomic, readonly) Class cacheResponseClass;

- (void) prepareRequest: (CTXActionInfoRequest*) request;

# pragma mark - GUI

@property (nonatomic, readonly) int channel;
@property (nonatomic, readonly) CTXPluginStyle * tileStyle;
@property (nonatomic, readonly) Class tileStyleClass;

@property (nonatomic, readonly) CTXPluginStyle * fullStyle;
@property (nonatomic, readonly) Class fullStyleClass;



- (CTXDashboardTileBaseViewController*) createTileViewControllerWithActionInfo:(CTXBaseActionInfoResponseBody*)actionInfo tileInfo:(CTXDashboardBlock *)tileInfo interaction: (CTXInteraction*) interaction;

- (CTXBaseActionViewController*) createFullViewControllerWithActionInfo:(CTXBaseActionInfoResponseBody*)actionInfo interaction: (CTXInteraction*) interaction;


@end
