//
//  CTXPlistResource.h
//  PathXP
//
//  Created by ftamagni on 22/03/16.
//  Copyright © 2016 01tribe. All rights reserved.
//

#import <CTXSDK/CTXBundleResource.h>

#define CTX_R_PLIST(_name_, _bundle_) [[CTXPlistResource alloc] initWithFileName:(_name_) bundle:(_bundle_)]
#define CTX_R_APP_PLIST(_name_) [[CTXPlistResource alloc] initWithFileName:(_name_) bundle:nil]

@interface CTXPlistResource : CTXBundleResource

- (instancetype) initWithFileName: (NSString*) fileName
                           bundle: (NSBundle*) bundle;

@end
