//
//  CTXSystemManager.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <CTXSDK/CTXPlistResource.h>

@interface CTXSystemManager : NSObject

#pragma mark - SharedInstance
+ (instancetype)manager;

+ (BOOL)runningInBackground;
+ (BOOL)runningInForeground;
+ (BOOL)isInactive;

+ (NSInteger)getBeaconPowerLenghtBufferWithProximity:(CLProximity)proximity;
+ (NSInteger)getInteractionThresholdToStartWithProximity:(CLProximity)proximity;


#pragma mark - Properties
@property (nonatomic) NSString *apiVersion;
@property (nonatomic) CTXPlistResource * configPlist;
@property (nonatomic) NSBundle *pathXPBundle;
@property (nonatomic) NSString *pushServer;
@property (nonatomic) NSString *accountName;
@property (nonatomic) NSString *appKey;
@property (nonatomic) BOOL pushEnabled;
@property (nonatomic, readonly) BOOL sdkCachingEnabled;
@property (nonatomic, readonly) NSTimeInterval updateTriggersInterval;
@property (nonatomic, readonly) NSTimeInterval updateCacheInterval;
@property (nonatomic, readonly) BOOL precacheMode;
@property (nonatomic) NSDate *serverDate;
@property (nonatomic) BOOL usePreciseGeofence;
@property (nonatomic) BOOL gpsLocationOnGeofence;
@property (nonatomic) BOOL forceTriggerUpdateOnNotifications;
@property (nonatomic) BOOL hardStartInteractionWithId;
@property (nonatomic) BOOL useCertificatePinning;
@property (nonatomic) BOOL pushSubscribeOnSignificantLocationChanges;
@property (nonatomic) BOOL avoidBackgroundPushSubscribeOnSignificantLocationChanges;
@property (nonatomic) BOOL beaconsCloseWhenIsFar;
@property (nonatomic, readonly) NSTimeInterval defaultBeaconPauseInterval;
@property (nonatomic, readonly) NSTimeInterval defaultGeofencePauseInterval;
@property (nonatomic, readonly) NSTimeInterval notificationsDebounceInterval;
@property (nonatomic, readonly) BOOL debounceNotificationsDaily;
@property (nonatomic, readonly) BOOL useNotificationActionCache;
@property (nonatomic, readonly) NSString* baseURI;
@property (nonatomic, readonly) BOOL keepGeofenceInteractionClosed;
@property (nonatomic, readonly) BOOL keepBeaconInteractionClosed;
@property (nonatomic, readonly) NSTimeInterval beaconsNotificationIntervall;
// showAlwaysLocalPush

@property (nonatomic, assign) NSInteger currentSessionId;


@end
