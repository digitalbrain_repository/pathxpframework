//
//  FriwixLoginManager.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//
#import <CTXSDK/CTXLoginPluginManager.h>

@interface FriwixLoginManager : CTXLoginPluginManager

@end
