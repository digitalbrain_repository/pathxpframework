//
//  CLLogger.h
//  CoreLibrary
//
//  Created by Massimiliano Schinco on 01/09/16.
//  Copyright © 2016 01tribe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
typedef NS_OPTIONS(NSUInteger, CLLoggerType) {
    CLLoggerTypeNone            =   0,
    CLLoggerTypeError           =   1 << 0,
    CLLoggerTypeWarning         =   1 << 1,
    CLLoggerTypeInfo            =   1 << 2,
    CLLoggerTypeService         =   1 << 3,
    CLLoggerTypeBeacon          =   1 << 4,
    CLLoggerTypeGeofance        =   1 << 5,
    CLLoggerTypeCache           =   1 << 6,
    CLLoggerTypeActions         =   1 << 7,
    CLLoggerTypeHardDebug       =   1 << 8,
    CLLoggerTypeVeryHardDebug   =   1 << 9
};


typedef enum  NSUInteger {
    
    CLLoggerLevelNone = 0,
    
    CLLoggerLevelBase = 1,
    
    CLLoggerLevelServiceDebug = 2,
    
    CLLoggerLevelAllDebug = 3,
    
}CLLoggerLevel;

@interface CLLogger : NSObject
+ (instancetype)sharedInstance;

@property (nonatomic, strong) UITextView *debugLbl;
@property (nonatomic) BOOL hideLoggerType;
@property (nonatomic) BOOL logOnVIewEnabled;
@property (nonatomic, strong) UIWindow *debugWin;
@property (nonatomic) CLLoggerType loggerType;
@property (nonatomic) CLLoggerLevel loggerLevel;


@end

void CLLogXObjc( CLLoggerType type, NSString *prefix, NSDictionary *object);
void CLLogX(CLLoggerType type, NSString *format, ...);
