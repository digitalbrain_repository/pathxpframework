//
//  CTXPlugin.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>
#import <CTXSDK/CTXPluginSDKInterface.h>

@protocol CTXPluginProtocol <NSObject>

@optional
- (void) onPluginLoaded;

@end

@interface CTXPlugin : NSObject <CTXPluginProtocol>

@property (nonatomic, readonly, strong) CTXPluginSDKInterface* sdkInterface;
@property (nonatomic, readonly) NSString * pluginBundleName;
@property (nonatomic, readonly) NSBundle * pluginBundle;

@end
