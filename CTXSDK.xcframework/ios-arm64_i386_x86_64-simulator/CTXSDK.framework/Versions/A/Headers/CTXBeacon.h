//
//  CTXBeacon.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>
#import <CTXSDK/CLJastor.h>
#import <CoreLocation/CoreLocation.h>

@interface CTXBeacon : CLJastor

#pragma mark - Functions
- (CLLocationAccuracy)distanceFromBeacon:(CTXBeacon*)otherBeacon;
- (BOOL)isInRangeWithDiscoveredBeacon:(CLBeacon*)discoveredBeacon;

#pragma mark - Properties from Server
@property (nonatomic, copy) NSString *distance; // cioè proximity
@property (nonatomic, copy) NSString *macAddress;
@property (nonatomic, copy) NSNumber *requiredAccuracy;
/*Currently hardcoded value in future this value will be settet by server*/
@property (nonatomic) BOOL closeWhenIsFar;
#pragma mark - Custom Properties
@property (nonatomic, readonly) CLProximity proximity;

#pragma mark - Properties from Beacon
@property (nonatomic) CLLocationAccuracy accuracy;
@property (nonatomic) NSInteger rssi;

@property (nonatomic) NSUUID * family;
@property (nonatomic) int major;
@property (nonatomic) int minor;
@property (nonatomic) NSUInteger vendorHash;
@property (nonatomic) NSString * vendor;

- (BOOL) equalTo: (CLBeacon*) clBeacon;

@end
