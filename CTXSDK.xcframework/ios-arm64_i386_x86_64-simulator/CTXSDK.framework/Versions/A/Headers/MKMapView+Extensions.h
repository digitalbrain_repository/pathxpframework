//
//  MKMapView+Extensions.h
//  Friwix
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <MapKit/MapKit.h>

@interface MKMapView (Extensions)

- (void)adjustToContainRect:(CGRect)rect usingReferenceView:(UIView *)referenceView withInsets: (UIEdgeInsets) insets animated:(BOOL)animated;

@end
