//
//  CTXTileTitleView.h
//  CTXSDK
//
//  Created by Massimiliano on 22/03/18.
//  Copyright © 2018 01tribe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CTXSDK/CLAdvancedAttributedLabel.h>
#import <CTXSDK/CTXDashboardEntity.h>
#import <CTXSDK/CTXGenericTileStyle.h>
@interface CTXTileTitleView : UIView
@property (nonatomic, weak) IBOutlet CLAdvancedAttributedLabel *titleLbl;
@property (nonatomic, weak) IBOutlet UILabel *subTitleLbl;
@property (nonatomic, weak) IBOutlet UIView *separatorRight;
@property (nonatomic, weak) IBOutlet UIView *separatorLeft;
@property (nonatomic, weak) IBOutlet UIView *separatorCenter;
+ (CTXTileTitleView *)addToSuperview:(UIView *)superview withTileInfo:(CTXDashboardBlock *)tileInfo withStyle: (CTXGenericTileStyle *) style;
- (void)setTextColor: (UIColor *)color;
@end
