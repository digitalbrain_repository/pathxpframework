//
//  CTXJustTriggerManager.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <CTXSDK/CTXBaseTriggerManager.h>

@interface CTXJustTriggerManager : CTXBaseTriggerManager

// *** SHARED INSTANCE ***
+ (instancetype)manager;

@end
