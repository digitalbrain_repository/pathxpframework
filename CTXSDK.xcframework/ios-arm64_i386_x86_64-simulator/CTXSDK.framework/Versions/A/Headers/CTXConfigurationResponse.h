//
//  CTXConfigNonceResponse.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>
#import <CTXSDK/C345Response.h>

@interface CTXConfigurationShowInteraction : CLJastor
@property (nonatomic, copy) NSNumber *interactionId;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *startDate;
@property (nonatomic, copy) NSString *endDate;
@end

#pragma mark - Class CTXConfigurationResponseBody
@interface CTXConfigurationResponseBody : CTXResponseBody

#pragma mark - Properties
@property (nonatomic, copy) NSString *nonce;
@property (nonatomic, copy) NSString *additionalInfo1;
@property (nonatomic, copy) NSString *additionalInfo2;
@property (nonatomic, copy) NSString *additionalInfo3;
@property (nonatomic, copy) NSString *statisticEndPoint;
@property (nonatomic, copy) NSString *statisticApiKey;
@property (nonatomic, strong) CTXConfigurationShowInteraction *showInteraction;

@end

#pragma mark - Class CTXConfigurationResponse
@interface CTXConfigurationResponse : C345Response

#pragma mark - Properties
@property (nonatomic) CTXConfigurationResponseBody *body;
@property (nonatomic) NSString *warningMessage;

@end
