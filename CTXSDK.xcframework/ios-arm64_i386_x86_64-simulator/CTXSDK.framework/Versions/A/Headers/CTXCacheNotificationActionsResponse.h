//
//  CTXCacheNotificationActionsReponse.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <CTXSDK/C345Response.h>
#import <CTXSDK/CTXCacheBaseActionsResponseBody.h>

@interface CTXCacheNotificationActionsResponseBody : CTXCacheBaseActionsResponseBody

#pragma mark - Properties
@property (nonatomic, strong) NSArray *notificationActions;

@end

@interface CTXCacheNotificationActionsResponse : C345Response

#pragma mark - Properties
@property (nonatomic) CTXCacheNotificationActionsResponseBody *body;

@end
