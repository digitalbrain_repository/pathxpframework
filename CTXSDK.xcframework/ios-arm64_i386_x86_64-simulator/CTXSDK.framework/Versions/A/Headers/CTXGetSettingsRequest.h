//
//  CTXGetSettingsRequest.h
//  PathXP
//
//  Created by Massimiliano on 04/07/17.
//  Copyright © 2017 01tribe. All rights reserved.
//

#import <CTXSDK/ContextSDK.h>
#define CTX_GET_SETTINGS_ACTION @"actionGetSettings"
#define CTX_PROFILE_WORKFLOW @"profileWorkflow"
@interface CTXGetSettingsRequest : C345Request <CTXRequestDelegate>

@end
