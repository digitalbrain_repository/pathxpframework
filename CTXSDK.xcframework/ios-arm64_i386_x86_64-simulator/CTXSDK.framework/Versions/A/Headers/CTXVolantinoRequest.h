//
//  CTXVolantinoRequest.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <CTXSDK/C345Request.h>

#pragma mark - Defines

#define CTX_VOLANTINO_ACTION        @"actionGetVolantino"

@interface CTXVolantinoRequest : C345Request <CTXRequestDelegate>

#pragma mark - Inizializer
- (instancetype)initWithWorkflowName:(NSString*)workflowName;

@end
