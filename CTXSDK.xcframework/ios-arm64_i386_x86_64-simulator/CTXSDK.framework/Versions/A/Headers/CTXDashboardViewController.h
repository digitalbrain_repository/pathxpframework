//
//  CTXDashboardViewController.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <UIKit/UIKit.h>
#import <CTXSDK/CTXDashboardEntity.h>
#import <CTXSDK/CTXDashboardTileView.h>
#import <CTXSDK/CoreLibrary.h>
#import <CTXSDK/CTXInteraction.h>
#import <CTXSDK/CTXDashboardSettings.h>
#import <CTXSDK/CTXStoryNavigatorEmbeddableController.h>
#import <CTXSDK/CTXGenericTileViewController.h>
#define CTX_DASHBOARD_VIEWCONTROLLER_IDENTIFIER   @"CTXDashboardVCIdentifier"
@interface CTXPulseView: UIView
- (void)start;
- (void)stop;
@end

@interface NSLayoutConstraint (Multiplier)
-(instancetype)updateMultiplier:(CGFloat)multiplier;
@end



@interface CTXDashboardViewController : CTXStoryNavigatorEmbeddableController <CTXDashboardTileProtocol>

#pragma mark - Properties
@property (nonatomic) CTXDashboardEntity *dashboardInfo;
@property (nonatomic) CTXInteraction *interaction;
@property (nonatomic) CTXDashboardSettings *customSettings;
/*
@property (nonatomic) CGFloat marginsWidth;
@property (nonatomic, getter = isClosable) BOOL closable;
 */

#pragma mark - Outlets
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet UIButton *buttonClose;

#pragma mark - Actions
- (IBAction)buttonCloseAction:(id)sender;

#pragma mark - Functions
- (void)dismissDashboardWithCompletion:(VoidBlock)handler;

- (void)buildDashBoardAuto;
- (void) hideLoadingView;

- (void) openTileWithActionId: (NSNumber*) actionId;

- (void)bringTileToFullFrom:(CTXGenericTileViewController *)tile to:(UIViewController *)target ;

- (void)backFromFullscreen:(UIViewController *)fullScreen;
@end


