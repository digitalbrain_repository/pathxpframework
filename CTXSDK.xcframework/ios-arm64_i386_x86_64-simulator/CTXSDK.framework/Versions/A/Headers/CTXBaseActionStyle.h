//
//  CTXBaseActionStyle.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <CTXSDK/CTXPluginStyle.h>

@protocol CTXBaseActionStylable <CTXPluginStylable>

@property (nonatomic) IBOutletCollection(UIButton) NSMutableArray * stylableClose;
@property (nonatomic) IBOutletCollection(UIView) NSMutableArray * stylableBg;
@property (nonatomic) IBOutletCollection(UILabel) NSMutableArray * stylableTitle;
@property (nonatomic) IBOutletCollection(UIButton) NSMutableArray * stylableButton;

@end

@interface CTXBaseActionStyle : CTXPluginStyle

// close icon
@property (nonatomic) CTXImageResource * closeIco;

// background color
@property (nonatomic) UIColor * bgColor;

// title style
@property (nonatomic) CTXFontResource * titleFont;
@property (nonatomic) UIColor * titleTextColor;

// call to action style
@property (nonatomic) UIColor * btTextColor;
@property (nonatomic) UIColor * btBgColor;
@property (nonatomic) CTXFontResource * btFont;

-(void)applyTo:(id<CTXBaseActionStylable>)stylable;

@end
