//
//  CTXStarRegionManager.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <CTXSDK/CTXBaseLocatorManager.h>
#import <UIKit/UIKit.h>

@protocol CTXStarRegionManagerDelegate <NSObject>

- (void) ctxStarRegionManagerDidChangeRegions;

@end


@interface CTXStarRegionManager : CTXBaseLocatorManager

@property (nonatomic, weak) id<CTXStarRegionManagerDelegate> locatorDelegate;

+ (instancetype)manager;

- (void) ensureTriggers;
- (void) ensureTriggersWithCompletionHandler: (void(^)(void)) handler;

- (void)didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
- (void)applicationDidEnterBackground:(UIApplication *)application;
- (void)applicationWillEnterForeground:(UIApplication *)application;

- (CLLocation*) bestLocation;

@end
