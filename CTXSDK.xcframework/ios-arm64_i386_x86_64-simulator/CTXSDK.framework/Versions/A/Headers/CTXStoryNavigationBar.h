//
//  CTXStoryNavigationBar.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <UIKit/UIKit.h>

@class CTXStoryNavigationBar;

@protocol CTXStoryNavigationBarDelegate <NSObject>

- (void) storyNavigationBarGoRoot: (CTXStoryNavigationBar*) bar;
- (void) storyNavigationBarGoPrev: (CTXStoryNavigationBar*) bar;
- (void) storyNavigationBarGoNext: (CTXStoryNavigationBar*) bar;
- (void) storyNavigationBarClose: (CTXStoryNavigationBar*) bar;

@end

@interface CTXStoryNavigationBar : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *prevButton;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;

@property (weak, nonatomic) id<CTXStoryNavigationBarDelegate> delegate;

@property (nonatomic, assign) BOOL hasNext;
@property (nonatomic, assign) BOOL hasPrev;
@property (nonatomic, assign) BOOL buttonsVisible;
@property (nonatomic, assign) BOOL closingEnabled;

@property (nonatomic, assign) BOOL buttonsEnabled;

@end
