//
//  ContextLoginDelegate.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>

@class CTXLoginRequest;

@protocol CTXLoginDelegate <NSObject>

@property (nonatomic) CTXLoginRequest *loginRequest;

@end
