//
//  CTXLocatorManagerPlus.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <CTXSDK/CTXBaseLocatorManager.h>

#import <UIKit/UIKit.h>

@protocol CTXLocatorManagerDelegate <NSObject>

- (void) ctxLocatorManagerDidChangeRegions;

@end

@interface CTXLocatorManager : CTXBaseLocatorManager

/**
 *  Singleton Instance
 *
 *  @return Ritorna la singleton instance del CTXLocatorManager
 */
+ (instancetype)manager;
+ (instancetype)sharedManager;

/**
 *  Ultima posizione conosciuta. Se nessuna, allora restituisce
 */
@property (nonatomic, readonly) CLLocation *lastLocation;

@property (nonatomic, weak) id<CTXLocatorManagerDelegate> locatorDelegate;

- (void)didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
- (void)applicationDidEnterBackground:(UIApplication *)application;

- (void) ensureTriggers;
- (void) ensureTriggersWithCompletionHandler: (void(^)(void)) handler;

@end
