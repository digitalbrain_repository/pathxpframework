//
//  CTXPluginManager.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>

@class CTXPlugin;
@class C345Action;
@class CTXActionPlugin;

@interface CTXPluginManager : NSObject

+ (instancetype) sharedInstance;

- (void) registerPluginClass: (Class) pluginClass;

- (CTXActionPlugin*) pluginWithAction: (C345Action*) action;
- (CTXActionPlugin*) actionPluginWithActionType: (NSString*) actionType;

- (BOOL) isActionSupported: (C345Action*) action;
- (void) forEachActionPlugin: (void(^)(CTXActionPlugin* plugin)) handler;

@end
