//
//  ContextLoginManager.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>
#import <CTXSDK/CTXLoginResponse.h>
#import <CTXSDK/CTXLoginRequest.h>
#import <CTXSDK/CTXLoginDelegate.h>
#import <CTXSDK/CTXSocialManager.h>
#import <CTXSDK/C345Response.h>
#import <CTXSDK/CTXServiceManager.h>
#import <CTXSDK/CTXLogoutResponse.h>
#import <CTXSDK/CoreLibrary.h>

@protocol CTXAuthenticationDelegate <NSObject>

@optional
- (void)loginSuccessful:(CTXLoginResponse*)loginResponse;
- (void)loginSuccessful:(CTXLoginResponse*)loginResponse completionHandler:(VoidBlock)handler;
- (void)loginFailed:(C345Response*)loginFailed;
- (void)refreshLoginWithSuccess:(VoidBlock)successHandler failure:(VoidBlock)failureHandler;
- (void)logoutSuccessful;

@end

@interface CTXLoginManager : NSObject

#pragma mark - SharedInstance
+ (instancetype)manager;
+ (instancetype)sharedManager;

@property (nonatomic) id<CTXAuthenticationDelegate> delegate;
@property (nonatomic) id<CTXAuthenticationDelegate> applicationDelegate;

#pragma mark - Properties
@property (nonatomic, readonly) BOOL userLogged;

#pragma mark - *** FUNCTIONS ***
- (void)loginSocialWithType:(ContextSocialType)socialType
                   delegate:(id<CTXSocialManagerDelegate>)delegate;

- (void)loginContextWithUsername:(NSString*)username
                     andPassword:(NSString*)password
                         success:(CTXLoginSuccessResponseHandler)success
                         failure:(CTXFailedResponseHandler)failure;

- (void)loginContextInFakeAccessWithSuccess:(CTXLoginSuccessResponseHandler)success
                                    failure:(CTXFailedResponseHandler)failure;

- (void)loginContextInFakeAccessWithUserUDID:(NSString*)userUDID
                           andAdditionalInfo:(NSString*)additionalInfo
                                     success:(CTXLoginSuccessResponseHandler)success
                                     failure:(CTXFailedResponseHandler)failure;

- (void)loginCustomerWithUsername:(NSString*)username
                      andPassword:(NSString*)password
                  additionalInfo1:(NSString*)additionalInfo1
                  additionalInfo2:(NSString*)additionalInfo2
                          success:(CTXLoginSuccessResponseHandler)success
                          failure:(CTXFailedResponseHandler)failure;

- (void)loginCustomerWithRequest:(id<CTXLoginDelegate>)request
                         success:(CTXLoginSuccessResponseHandler)success
                         failure:(CTXFailedResponseHandler)failure;

- (void)logoutContextWithSuccess:(CTXLogoutSuccessResponseHandler)success
                         failure:(CTXFailedResponseHandler)failure;

- (void)refreshLoginWithSuccess:(VoidBlock)successHandler failure:(VoidBlock)failureHandler;

- (void)removeFakeAccessCredentials;
- (void)resetLoggedState;

@end
