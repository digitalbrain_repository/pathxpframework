//
//  CryptingManager.h
//  CoreLibrary
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>

@interface CLCrypting : NSObject

#pragma mark - Functions
+ (NSString *)md5FromString:(NSString *)string;
+ (NSString *)sha256FromString:(NSString *)string;
+ (NSString *)base64FromString:(NSString *)string encodeWithNewlines:(BOOL)encodeWithNewlines;
+ (NSString *)signWithRSASha1:(NSString *)publicKey privateKey:(NSString*)privateKeyBase64;

+ (NSString *)md5FromData:(NSData *)data;

@end
