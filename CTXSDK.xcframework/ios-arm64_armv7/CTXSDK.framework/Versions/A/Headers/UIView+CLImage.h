//
//  UIView+CLImage.h
//  CoreLibrary
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <UIKit/UIKit.h>

@interface UIView (CLImage)

-(UIImage *)convertToImage;

@end
