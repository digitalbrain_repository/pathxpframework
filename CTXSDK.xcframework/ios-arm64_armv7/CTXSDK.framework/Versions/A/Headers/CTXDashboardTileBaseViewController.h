//
//  CTXDashboardTileBaseViewController.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <UIKit/UIKit.h>
#import <CTXSDK/CTXChannelEntry.h>
#import <CTXSDK/CTXDashboardEntity.h>
#import <CTXSDK/CTXGenericTileStyle.h>

@class CTXStoryNavigatorController, CTXDashboardBlock, CTXActionPlugin;

@interface CTXDashboardTileBaseViewController : UIViewController<CTXGenericTileStylable>


- (NSString*) resolveTitle: (NSString*) externalTitle;
- (NSString*) resolveSubtitle: (NSString*) externalSubtitle;
- (NSNumber*) resolvePreviewId: (NSNumber*) externalPreviewId;



/**
 *  Inject the title programmatically, with constraints and separator.
 *
 *  @param titleText string of text
 */
- (UILabel *) addTitleWithText: (NSString*) titleText;

/**
 *  Inject the subTitle programmatically, with constraints and separator.
 *
 *  @param subTitleText string of text
 */
- (UILabel *) addSubTitleWithText: (NSString*) subTitleText;

/**
 *  Add an icon constrained below the title.
 *
 */
- (UIImageView*) addContentTypeIcon: (CTXImageResource*) iconResource;

- (void) hideTileOverlays;

- (void) showTileOverlays;

- (UIViewController*) fullParentController;
- (CTXStoryNavigatorController*) parentStoryNavigator;
- (BOOL) avoidKenShiro;

@property (nonatomic) CTXChannelEntry * channelEntry;

@property (nonatomic) CTXDashboardBlock *tileInfo;

@property (nonatomic, weak) CTXActionPlugin *responsibleActionPlugin;

@property (nonatomic) CTXGenericTileStyle * style;

@property (nonatomic) IBOutletCollection(UIView) NSMutableArray * stylableBg;
@property (nonatomic) IBOutletCollection(UIView) NSMutableArray * stylableSeparator;
@property (nonatomic) IBOutletCollection(UILabel) NSMutableArray * stylableTitle;
@property (nonatomic) IBOutletCollection(UILabel) NSMutableArray * stylableDescription;


@end
