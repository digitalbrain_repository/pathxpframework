//
//  CTXBaseTriggerManager.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>
#import <CTXSDK/CTXTrigger.h>

@interface CTXBaseTriggerManager : NSObject

#pragma mark - Properties
@property (nonatomic) NSArray *triggers;

/**
 *  Controlla l'esistenza di un trigger che sia associato all'interactionId passato in input
 *
 *  @param interactionId Id dell'interazione associata al trigger da controllare
 *
 *  @return Ritorna vero o falso a seconda che esista o meno un trigger associato
 */
- (BOOL)existsTriggerWithInteractionId:(NSNumber*)interactionId;

/**
 *  Restituisce il trigger associato all'interactionId passato in input
 *
 *  @param interactionId Id dell'interazione associata al trigger da resitutire
 *
 *  @return Ritorna il trigger associato all'interazione identificata dall'interactionId in input, nil altrimenti
 */
- (CTXTrigger*)getTriggerWithInteractionId:(NSNumber*)interactionId;

- (BOOL) triggersAreNearInTime;

- (CTXTrigger*) mostRecentValidTrigger;
- (NSArray*) allValidNoTriggers;


@end
