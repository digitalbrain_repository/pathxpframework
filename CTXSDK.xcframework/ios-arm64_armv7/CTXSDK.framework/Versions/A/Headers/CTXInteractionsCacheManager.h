//
//  CTXInteractionsCacheManager.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>
#import <CTXSDK/CTXInteractionInfoResponse.h>
#import <CTXSDK/CTXServiceManager.h>
#import <CTXSDK/CTXBaseActionInfoResponseBody.h>
#import <CTXSDK/CTXInteraction.h>

#pragma mark - Defines
#define CTX_CACHE_WORKFLOW    @"cacheWorkflow"

@interface CTXInteractionsCacheManager : NSObject

#pragma mark - Shared Instance
+ (instancetype)manager;

- (void)refreshWithSuccess:(CTXSuccessResponseHandler)success
                    failed:(CTXFailedResponseHandler)failure;

- (NSArray*)getInteractions;
- (void)save;

- (CTXInteractionInfoResponseBody*)getInteractionInfoWithInteractionId:(NSNumber*)interactionId;
- (void)setInteractionInfo:(CTXInteractionInfoResponseBody*)interactionInfo withInteractionId:(NSNumber*)interactionId;
- (void)setActionInfo:(CTXBaseActionInfoResponseBody *)actionInfo withType:(NSString*)actionType;

- (CTXBaseActionInfoResponseBody*)getActionInfoWithId:(NSNumber*)actionId withType:(NSString*)actionType;

- (NSArray*)getActionsWithType:(NSString*)actionType;

@end
