//
//  CTXGenericTileStyle.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <CTXSDK/CTXPluginStyle.h>


@protocol CTXGenericTileStylable <CTXPluginStylable>

@property (nonatomic) IBOutletCollection(UIView) NSMutableArray * stylableBg;
@property (nonatomic) IBOutletCollection(UIView) NSMutableArray * stylableSeparator;
@property (nonatomic) IBOutletCollection(UILabel) NSMutableArray * stylableTitle;
@property (nonatomic) IBOutletCollection(UILabel) NSMutableArray * stylableDescription;

@end


@interface CTXGenericTileStyle : CTXPluginStyle

@property (nonatomic) UIColor * bgColor;
@property (nonatomic) UIColor * separatorColor;
@property (nonatomic) UIColor * titleColor;
@property (nonatomic) UIColor * descriptionColor;

@property (nonatomic) CTXFontResource * titleFont;
@property (nonatomic) CTXFontResource * descriptionFont;

@property (nonatomic) CTXImageResource * typeIcon;

-(void)applyTo:(id<CTXGenericTileStylable>)stylable;

@end
