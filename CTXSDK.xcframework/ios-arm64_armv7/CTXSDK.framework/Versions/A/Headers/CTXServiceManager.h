//
//  CTXServiceManager.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>
#import <CTXSDK/CLServicesManager.h>
#import <CTXSDK/C345Response.h>
#import <CTXSDK/C345Request.h>
#import <CTXSDK/CoreLibrary.h>


#pragma mark - Protocol CTXServiceDelegate
typedef NS_ENUM(NSInteger, CTXServiceState)
{
    CTXServiceStateReady = 0,
    CTXServiceStateInitializing,
    CTXServiceStateError
};

typedef void (^CTXServiceDelegateHandler)(CTXServiceState serviceState, NSString *message);
typedef void (^CTXSuccessResponseHandler)(NSURLSessionDataTask *operation, C345Response *responseObject);
typedef void (^CTXFailedResponseHandler)(NSURLSessionDataTask *operation, C345Response *responseObject);
//SWIFT COMPATIBILITY
typedef void (^CTXDicitionaryResponseHandler) (NSURLSessionDataTask *operation, NSDictionary *responseDictionary);

@protocol CTXServiceDelegate <NSObject>

@optional

- (void)serviceIsReadyForRequest:(C345Request*)request completionHandler:(CTXServiceDelegateHandler)handler;
- (void)serviceRefreshSessionWithId: (NSInteger) sessionId success:(VoidBlock)successHandler failure:(VoidBlock)failureHandler;

@end




@interface CTXServiceManager : NSObject

#pragma mark - SharedInstance
+ (instancetype)manager;
+ (instancetype)shared;
#pragma mark - CTXServiceDelegate
@property (nonatomic) id<CTXServiceDelegate> delegate;

#pragma mark - Functions
- (NSURLSessionDataTask *)sendRequest:(NSString*)url
        requestType:(CLRequestType)requestType
      requestObject:(C345Request<CTXRequestDelegate>*)requestObject
      responseClass:(typeof([C345Response class]))responseClassType
            success:(CTXSuccessResponseHandler)success
             failed:(CTXFailedResponseHandler)failure;

- (NSURLSessionDataTask *)sendRequest:(NSString*)url
        requestType:(CLRequestType)requestType
    requestPriority:(CLBatchRequestPriorityLevel)requestPriority
      requestObject:(C345Request<CTXRequestDelegate>*)requestObject
      responseClass:(typeof([C345Response class]))responseClassType
            success:(CTXSuccessResponseHandler)success
             failed:(CTXFailedResponseHandler)failure;

//MARK: - Swift Compatibility
- (NSURLSessionDataTask *)sendRawRequest:(NSString*)url
                             requestType:(CLRequestType)requestType
                       requestDicitonary:(NSDictionary *)requestObject
                                 success:(CTXDicitionaryResponseHandler)success
                                  failed:(CTXDicitionaryResponseHandler)failure;

@end
