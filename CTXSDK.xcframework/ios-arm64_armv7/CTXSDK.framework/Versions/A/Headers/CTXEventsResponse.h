//
//  CTXEventsResponse.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <CTXSDK/C345Response.h>
#import <CTXSDK/CTXEvent.h>

#pragma mark - Class CTXVolantinoResponseBody
@interface CTXEventsResponseBody : CTXResponseBody

#pragma mark - Properties
@property (nonatomic, strong) NSArray *events;

@end

@interface CTXEventsResponse : C345Response

#pragma mark - Properties
@property (nonatomic) CTXEventsResponseBody *body;

@end
