//
//  CTXDashboardTileView.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <UIKit/UIKit.h>
#import <CTXSDK/CTXDashboardEntity.h>

@class CTXDashboardTileView;

@protocol CTXDashboardTileProtocol <NSObject>

- (void)handleTileSingleTap:(CTXDashboardTileView*)tile
             withRecognizer:(UITapGestureRecognizer*)gestureRecognizer;

@end

@interface CTXDashboardTileView : UIView

#pragma mark - Init FUnctions
- (id)initWithDelegate:(id<CTXDashboardTileProtocol>)delegate tileInfo:(CTXDashboardBlock*)tileInfo frame:(CGRect)frame;

#pragma mark - Properties
@property (nonatomic, readonly) CTXDashboardBlock *tileInfo;

@end
