//
//  CTXBaseActionInfoResponseBody.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <CTXSDK/C345Response.h>
#import <CTXSDK/CTXActionHeader.h>
#import <CTXSDK/CTXCallToAction.h>
@interface CTXBaseActionInfoResponseBody : CTXResponseBody

@property (nonatomic) NSNumber *updated;
@property (nonatomic) NSString* currentLayout;
@property (nonatomic) CTXCallToAction *globalCallToAction;
@property (nonatomic) CTXActionHeader* actionBar;

- (int) countImages;
- (void) cacheImagesWithCompletionHandler: (void(^)(void)) handler;
- (NSArray*) allImageIds;

@end
