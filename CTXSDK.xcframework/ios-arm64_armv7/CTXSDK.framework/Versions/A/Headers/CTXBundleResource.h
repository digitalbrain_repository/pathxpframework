//
//  CTXBundleResource.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <UIKit/UIKit.h>

@interface CTXBundleResource : NSObject

@property (readonly, strong) NSBundle * bundle;
@property (readonly, strong) NSString * what;

- (instancetype) initWithBundle: (NSBundle*) bundle;
- (id) load;

@end
