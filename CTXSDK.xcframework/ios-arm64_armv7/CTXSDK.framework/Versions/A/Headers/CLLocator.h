//
//  CLLocator.h
//  CoreLibrary
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface CLLocator : NSObject

#pragma mark - Functions

/**
 * Returns the current authorization status
 *
 * @return Current location authorization status for application
 */
+ (CLAuthorizationStatus) authorizationStatus;

/**
 * Returns if location services are enabled
 *
 * @return true if location services are enabled
 */
+ (BOOL) locationServicesEnabled;

/**
 * Returns if power-saving location services are available
 *
 * @return true if power-saving location services are available
 */
+ (BOOL) backgroundLocationChangeMonitoringAvailable;

/**
 * Returns a non-started standard location manager.
 *
 * Remember to start it after allocation, to receive update events.
 *
 * @param delegate The delegate object to receive update events.
 * @param desiredAccuracy The accuracy of the location data.
 * @param distanceFilter The minimum distance (measured in meters) a device must move horizontally before an update event is generated.
 * @param headingFilter The minimum angular change (measured in degrees) required to generate new heading events.
 *
 * @return A non-started standard location manager
 */
+ (CLLocationManager *) locationManagerWithDelegate:(id<CLLocationManagerDelegate>)delegate
                                    desiredAccuracy:(CLLocationAccuracy)desiredAccuracy
                                     distanceFilter:(CLLocationDistance)distanceFilter
                                      headingFilter:(CLLocationDegrees)headingFilter;

/**
 * Returns a started power-saving location manager that notifies only significant location changes.
 * 
 * If you start this service and your application is subsequently terminated, the system automatically relaunches the application into the background if a new event arrives.
 *
 * Apps can expect a notification as soon as the device moves 500 meters or more from its previous notification. It should not expect notifications more frequently than once every five minutes.
 *
 * @param delegate The delegate object to receive update events.
 *
 * @return A started background power-saving location manager
 */
+ (CLLocationManager *) backgroundLocationManagerWithDelegate:(id<CLLocationManagerDelegate>)delegate;

@end
