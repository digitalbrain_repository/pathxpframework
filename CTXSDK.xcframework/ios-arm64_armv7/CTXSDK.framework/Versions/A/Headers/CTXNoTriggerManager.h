//
//  CTXNoTriggerManager.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <CTXSDK/CTXBaseTriggerManager.h>

@interface CTXNoTriggerManager : CTXBaseTriggerManager

+ (instancetype)manager;

@end
