//
//  CTXInteractionInfoResponse.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <CTXSDK/C345Response.h>
#import <CTXSDK/CTXDashboardEntity.h>
#import <CTXSDK/C345Action.h>
#import <CTXSDK/CTXBaseActionInfoResponseBody.h>

#pragma mark - Class CTXInteractionInfoResponseBody
@interface CTXInteractionInfoResponseBody : CTXBaseActionInfoResponseBody

@property (nonatomic, strong) CTXDashboardEntity *dashboard;
@property (nonatomic, strong) NSArray *actions;
@property (nonatomic) BOOL cache;
@property (nonatomic) BOOL executeNow;

@end


#pragma mark - Class CTXInteractionInfoResponse
@interface CTXInteractionInfoResponse : C345Response

#pragma mark - Properties
@property (nonatomic) CTXInteractionInfoResponseBody *body;

@end
