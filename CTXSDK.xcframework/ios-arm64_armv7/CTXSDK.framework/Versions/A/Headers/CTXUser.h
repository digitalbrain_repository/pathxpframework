//
//  CTXUser.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>

@interface CTXUser : NSObject

@property (nonatomic) NSString *name;
@property (nonatomic) NSString *surname;

@end
