//
//  C345Request.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <CTXSDK/CLJastor.h>

#define CTX_USER_AGENT_VALUE        @"iOS";

@class CTXRequestHeader, CTXRequestBody;

#pragma mark - Protocol CTXRequestDelegate
@protocol CTXRequestDelegate <NSObject>

@required
@property (nonatomic) CTXRequestHeader *requestHeader;
@property (nonatomic) CTXRequestBody *body;

@end

#pragma mark - Class CTXRequestHeader
@interface CTXRequestHeader : CLJastor

@property (nonatomic) NSString *traceId;
@property (nonatomic) NSString *workflowName;
@property (nonatomic) NSString *actionName;
@property (nonatomic) NSString *userAgent;
@property (nonatomic) NSString *locale;
@property (nonatomic) NSString *lat;
@property (nonatomic) NSString *lon;
@property (nonatomic) NSString *apiVersion;
@end

#pragma mark - Class CTXRequestBody
@interface CTXRequestBody : CLJastor
@end

#pragma mark - Class C345Request
@interface C345Request : CLJastor

@property (nonatomic, readonly) NSInteger sessionId;

@end
