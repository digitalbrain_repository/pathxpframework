//
//  ContextMessagesDelegate.h
//  CoreLibrary
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>

@protocol CLMessagesDelegate <NSObject>

@property (nonatomic) NSString *genericErrorMsg;

@end
