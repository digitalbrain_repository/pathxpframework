//
//  CTXMedia.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//
#import <UIKit/UIKit.h>
#import <CTXSDK/CLJastor.h>
#import <CTXSDK/CTXCallToAction.h>

#pragma mark - Defines
typedef NS_ENUM(NSInteger, CTXMediaSourceType)
{
    CTXMediaSourceTypeCustom    = 0,
    CTXMediaSourceTypeWeb,
    CTXMediaSourceTypeYoutube,
    CTXMediaSourceTypeVimeo,
    CTXMediaSourceTypesCount
};

typedef NS_ENUM(NSInteger, CTXMediaType)
{
    CTXMediaTypeImage    = 0,
    CTXMediaTypeVideo,
    CTXMediaTypesCount
};

@interface CTXMediaCrop : CLJastor

#pragma mark - Properties
@property (nonatomic) NSArray *topLeft;
@property (nonatomic) NSArray *topRight;
@property (nonatomic) NSArray *bottomLeft;
@property (nonatomic) NSArray *bottomRight;

@end

@interface CTXMedia : CLJastor

#pragma mark - Properties
@property (nonatomic) NSString *type;
@property (nonatomic) NSString *source;
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *descript;
@property (nonatomic) BOOL showText;
@property (nonatomic) NSNumber *previewImageId;
@property (nonatomic) NSNumber *imageId;
@property (nonatomic) NSString *url;
@property (nonatomic) NSNumber *order;
@property (nonatomic) CTXMediaCrop *cropSquare;
@property (nonatomic) CTXMediaCrop *cropVertical;
@property (nonatomic) CTXMediaCrop *cropHorizontal;
@property (nonatomic) CTXCallToAction *callToAction;
@property (nonatomic) NSArray <CTXCallToAction *> * callToActions;
@property (nonatomic) BOOL downloadable;


@property (nonatomic) NSString *bgColor;
@property (nonatomic) NSString *descriptionColor;
@property (nonatomic) NSString *titleColor;
#pragma mark - Custom Properties
//@property (nonatomic) UIImage *image;
//@property (nonatomic) UIImage *previewImage;

#pragma mark - Custom Properties ReadOnly
@property (nonatomic, readonly) CTXMediaType typeMediaType;
@property (nonatomic, readonly) CTXMediaSourceType sourceType;

- (BOOL) isDataValid;

@end
