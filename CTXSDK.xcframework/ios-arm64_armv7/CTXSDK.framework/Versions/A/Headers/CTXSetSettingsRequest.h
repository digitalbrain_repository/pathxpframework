//
//  CTXSetSettingsRequest.h
//  PathXP
//
//  Created by Massimiliano on 04/07/17.
//  Copyright © 2017 01tribe. All rights reserved.
//

#import <CTXSDK/ContextSDK.h>
#import <CTXSDK/CTXGetSettingsRequest.h>
#import <CTXSDK/CTXGetSettingsResponse.h>
#define CTX_SET_SETTINGS_ACTION @"actionSetSettings"

@interface CTXSetSettingsRequestBody: CTXRequestBody
@property (nonatomic) NSArray <CTXSettingsOption *> *options;
@end

@interface CTXSetSettingsRequest : C345Request <CTXRequestDelegate>
@property (nonatomic) CTXSetSettingsRequestBody *body;
@end
