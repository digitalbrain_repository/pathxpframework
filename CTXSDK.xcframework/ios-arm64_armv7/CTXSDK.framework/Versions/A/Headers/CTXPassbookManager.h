//
//  CTXPassbookManager.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>
#import <CTXSDK/CTXPassbookRequest.h>
#import <CTXSDK/CTXServiceManager.h>
#import <CTXSDK/CTXPassbookResponse.h>

#pragma mark - Defines
typedef void (^CTXPassbookSuccessResponseHandler)(NSURLSessionDataTask *operation, CTXPassbookResponse *responseObject);

@interface CTXPassbookManager : NSObject

+ (void)generatePassbookWithInfo:(CTXPassbookRequest*)passbookRequest
                         success:(CTXPassbookSuccessResponseHandler)success
                         failure:(CTXFailedResponseHandler)failure;

@end
