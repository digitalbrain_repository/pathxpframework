//
//  CTXBeaconsHelper.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <CTXSDK/CTXBeacon.h>

@interface CTXBeaconsHelper : NSObject

@property (nonatomic, strong) NSMutableDictionary *smottedItems;
// restituisce un dictionary di beacons a cui siamo interessati e che passano le verifiche
+ (NSDictionary*)getBeaconsOfInterestWithBeaconsToDiscover:(NSDictionary*)beaconsToDiscover withBeaconsInRanging:(NSArray*)beaconsInRanging;
+ (NSString*)macAddressWithBeacon:(CLBeacon*)beacon;
+ (BOOL)arrayContains:(NSArray *)beacons beacon:(CLBeacon *)beacon;
+ (NSString *)keyFromBeacon:(CLBeacon *)beacon;
+ (instancetype)sharedInstance;
@property (nonatomic) int farLimit;
@property (nonatomic) int nearLimit;
@end


@interface CTXSmooterItem: NSObject
@property (nonatomic,strong) CLBeacon *originalBeacon;
@property (nonatomic, strong) CTXBeacon *originalCTX;
@property (nonatomic) NSMutableArray *rssix;
@property (nonatomic) double avarage;
- (id)initWithBeacon:(CLBeacon *)beacon;
- (void)updateWithBeacon:(CLBeacon *)beacon;
- (void)fade;

- (int)threeshold;
@end
