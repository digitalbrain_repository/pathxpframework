//
//  UIView+Constraints.h
//

#import <UIKit/UIKit.h>

@interface UIView (Constraints)

- (void) addSubviewConstrainted:(UIView *)view;
- (void) addSubviewConstrainted:(UIView *)view withMarginTop:(int)top bottom:(int)bottom left:(int)left right:(int)right;
- (void) addSubviewConstrainted2:(UIView *)view withMarginTop:(int)top bottom:(int)bottom left:(int)left right:(int)right;

- (void) addSubviewConstraintedInCenter:(UIView *)view;
- (void) addSubviewConstraintedInCenter:(UIView *)view withWidth:(int)width andHeight:(int)height;
- (void) addSubviewConstraintedHCentered:(UIView *)view fromTop:(int)top;
- (void) addSubviewConstraintedHCentered:(UIView *)view fromTop:(int)top withWidth:(int)width andHeight:(int)height;

- (void) addSubviewConstraintedInCenter:(UIView *)view withWidth:(float)width;

-(void)addHorizontalSubviewArray:(NSArray *)subviews withMargin:(float)margin;
-(void)addVerticalSubviewArray:(NSArray *)subviews withMargin:(float)margin;
-(void)addHorizontalSubviewArrayKeepSuperviewHeightForViewsArray:(NSArray *)subviews withMargin:(float)margin;
-(void)addVerticalSubviewArrayWithoutHeight:(NSArray *)subviews withMargin:(float)margin;

-(void)addsubviewCentered:(UIView *)view withHorizontalMarginPercent:(float)horMarginPerc verticalMarginPercent:(float)verticalMarginPercent andRatio:(float)ratio;


-(void)addSubviewConstraintedInCenter:(UIView *)view withWidthPercentage:(float)perc;

-(void)addVerticalSubview:(UIView *)subview withMargin:(float)margin;
-(void)addVerticalSubview:(UIView *)subview;

- (NSLayoutConstraint*) topConstraint;
- (NSLayoutConstraint*) bottomConstraint;

- (NSLayoutConstraint*) trailingConstraint;
- (NSLayoutConstraint*) leadingConstraint;

- (NSLayoutConstraint*) widthConstraint;
- (NSLayoutConstraint*) heightConstraint;

@end
