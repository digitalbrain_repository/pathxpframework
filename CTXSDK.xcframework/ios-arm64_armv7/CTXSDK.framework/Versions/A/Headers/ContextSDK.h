//
//  ContextSDK.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>

#pragma mark - Importing main header files

#import <CTXSDK/CTXMessagesManager.h>
#import <CTXSDK/CTXConfigurationManager.h>
#import <CTXSDK/CTXApplicationManager.h>
#import <CTXSDK/CTXPushNotificationManager.h>
#import <CTXSDK/CTXLoginManager.h>
#import <CTXSDK/CTXPluginManager.h>

@interface ContextSDK : NSObject <CTXAuthenticationDelegate>

#pragma mark - SharedInstance
+ (instancetype)sharedInstance;

#pragma mark - init ContextSDK
- (void)startEngine;
+ (void)startEngine;
- (void)startEngineWithSuccess:(ContextConfigSuccessResponseHandler)successhandler
                        failed:(CTXFailedResponseHandler)failedHandler;

@end
