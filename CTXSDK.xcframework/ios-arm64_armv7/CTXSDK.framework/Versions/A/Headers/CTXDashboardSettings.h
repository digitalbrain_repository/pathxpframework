//
//  CTXDashboardSettings.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//
#import <CoreGraphics/CoreGraphics.h>
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CTXDashboardSettings : NSObject

@property (nonatomic) CGFloat tilesDistance;
@property (nonatomic) CGFloat dashboardBorder;
@property (nonatomic, getter = isClosable) BOOL closable;
@property (nonatomic) UIColor *backgroundColor;
@property (nonatomic) NSNumber * supportedOrientationsMask;
@property (nonatomic) UIColor *tileBackgroundColor;
@property (nonatomic) UIActivityIndicatorViewStyle activityIndicatorViewStyle;
@end
