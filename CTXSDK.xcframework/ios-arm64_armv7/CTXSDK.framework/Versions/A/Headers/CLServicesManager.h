//
//  ContextServices.h
//  CoreLibrary
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>

#import <CTXSDK/AFHTTPSessionManager.h>

#pragma mark - Defines
typedef void (^CLSuccessResponseHandler)(NSURLSessionDataTask *operation, id responseObject);
typedef void (^CLFailedResponseHandler)(NSURLSessionDataTask *operation, NSError *error);

typedef NS_ENUM(NSInteger, CLRequestType)
{
    CLRequestTypeGET = 0,
    CLRequestTypePOST,
    CLRequestTypePATCH,
    CLRequestTypeDELETE,
    CLRequestTypeHEAD,
    CLRequestTypePUT
};

typedef NS_ENUM(NSInteger, CLSerializerType)
{
    CLSerializerTypeHTTP = 0,
    CLSerializerTypeJSON,
    CLSerializerTypeClass
};

typedef NS_ENUM(NSInteger, CLCommunicationType)
{
    CLCommunicationTypeAsynchronous = 0,
    CLCommunicationTypeSynchronous
};

typedef NS_ENUM(NSInteger, CLBatchRequestPriorityLevel)
{
    CLBatchRequestPriorityLevelHigh = 0,
    CLBatchRequestPriorityLevelDefault,
    CLBatchRequestPriorityLevelLow,
    CLBatchRequestPriorityLevelsCount
};

@interface CLServiceRequest : NSObject

@property (nonatomic) NSString *url;
@property (nonatomic) NSDictionary *headerParams;
@property (nonatomic) id requestParams;
@property (nonatomic) CLRequestType requestType;
@property (nonatomic) CLSerializerType requestSerializer;
@property (nonatomic) CLSerializerType responseSerializer;
@property (nonatomic) Class responseClassName;
@property (nonatomic) CLCommunicationType communicationType;
@property (nonatomic, copy) CLSuccessResponseHandler successBlock;
@property (nonatomic, copy) CLFailedResponseHandler failedBlock;

@end

@interface CLServicesManager : NSObject

#pragma mark - SharedInstance
+ (instancetype)manager;

#pragma mark - Functions
+ (NSURLSessionDataTask*)sendRequestWithUrl:(NSString*)url
                                 headerParams:(NSDictionary*)headerParams
                                requestParams:(id)_requestParams
                                  requestType:(CLRequestType)requestType
                            requestSerializer:(CLSerializerType)requestSerializer
                           responseSerializer:(CLSerializerType)responseSerializer
                                responseClass:(Class)responseClassName
                               cachingEnabled:(BOOL)cachingEnabled
                                      success:(void (^)(NSURLSessionDataTask *task, id responseObjec))_success
                                      failure:(void (^)(NSURLSessionDataTask *task, NSError *error))_failure;

+ (NSURLSessionDataTask*)sendRequestWithUrl:(NSString*)url
                                 headerParams:(NSDictionary*)headerParams
                                requestParams:(id)_requestParams
                                  requestType:(CLRequestType)requestType
                            requestSerializer:(CLSerializerType)requestSerializer
                           responseSerializer:(CLSerializerType)responseSerializer
                                responseClass:(Class)responseClassName
                                   executeNow:(BOOL)executeNow
                               cachingEnabled:(BOOL)cachingEnabled
                                      success:(void (^)(NSURLSessionDataTask *task, id responseObjec))_success
                                      failure:(void (^)(NSURLSessionDataTask *task, NSError *error))_failure;

+ (void)sendRequest:(CLServiceRequest*)request;


@end
