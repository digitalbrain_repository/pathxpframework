//
//  CTXVoltantinoResponse.h
//  ContextSDK
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <CTXSDK/C345Response.h>

#pragma mark - Class CTXVolantinoResponseBody
@interface CTXVolantinoResponseBody : CTXResponseBody

#pragma mark - Properties
@property (nonatomic) NSString *lastUpdate;
@property (nonatomic, strong) NSArray *pages;
@property (nonatomic) NSArray *images;

@end

@interface CTXVoltantinoResponse : C345Response

#pragma mark - Properties
@property (nonatomic) CTXVolantinoResponseBody *body;

@end
