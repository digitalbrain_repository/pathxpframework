
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define DATE_FULL_YEAR_FORMAT       @"dd/MM/yyyy"
#define DATE_STANDARD_FORMAT        @"yyyy-MM-dd"
#define DATE_VISUAL_FORMAT          @"dd MMM"
#define DATE_TIME_FORMAT            @"HH:mm"
#define DATE_LONG_FORMAT            @"yyyy-MM-dd HH:mm:ss.SSSS"
#define DATE_JSON_CONTEXT           @"dd/MM/yyyy HH:mm"
#define DATE_TIME_FORMAT_AMPM       @"hh:mm a"

@interface CLUtilities : NSObject

+ (NSString*)generaTraceID;
+ (NSString*)generaPrexifSuid;

+ (NSString*)euroNumberToEuroString:(NSNumber*)number withSymbol:(BOOL)displaySymbol;
+ (NSString*)centesimiNumberToEuroString:(NSNumber*)centesimiNumber withSymbol:(BOOL)displaySymbol;
+ (NSString*)centesimiStringToEuroString:(NSString*)centesimiString withSymbol:(BOOL)displaySymbol;
+ (NSNumber*)euroStringToEuroNumber:(NSString*)moneyString;
+ (NSNumber*)euroStringToCentesimiNumber:(NSString*)moneyString;
+ (NSString*)euroStringToCentesimiString:(NSString*)moneyString;
+ (BOOL)stringIsNumeric:(NSString*)inputString;

+ (NSDate*)dateFromString:(NSString*)dateString WithFormat:(NSString*)dateFormat;
+ (NSDate*)UTCDateFromString:(NSString*)dateString WithFormat:(NSString*)dateFormat timeZone: (int) timeZone;

+ (NSString*)stringFromDate:(NSDate*)date WithFormat:(NSString*)stringFormat;

+ (NSString*) bundlePathOf: (NSBundle*) bundle;
+ (NSString*)loadFontResource:(NSString*)fontFileName fromBundle: (NSBundle*) bundle;
+ (NSBundle*)retrieveMainBundle;
+ (BOOL)isIpad;
@end
