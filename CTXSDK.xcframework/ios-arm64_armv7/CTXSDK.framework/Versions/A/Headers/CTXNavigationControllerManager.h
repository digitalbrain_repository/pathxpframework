//
//  CTXNavigationControllerManager.h
//  CTXSDK
//
//  Created by Massimiliano on 26/02/18.
//  Copyright © 2018 01tribe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CTXSDK/CTXInteractionState.h>
#import <CTXSDK/CTXInteractionLink.h>

@interface CTXNavigationControllerManager : NSObject

+ (CTXNavigationControllerManager *)sharedInstance;

- (void) setChannelEntries: (NSArray*) entries forcedEntries: (NSArray*) forcedEntries onNavigationController: (UINavigationController *) navController withCompletionHandler: (void(^)(void)) handler;

- (void)pushLink:(CTXInteractionLink *)newLink toNavigationController:(UINavigationController *)nav;
@end
