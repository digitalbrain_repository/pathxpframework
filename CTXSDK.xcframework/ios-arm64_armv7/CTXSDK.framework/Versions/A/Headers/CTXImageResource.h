//
//  CTXImageResource.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <CTXSDK/CTXBundleResource.h>

#define CTX_R_IMG(_name_, _bundle_) [[CTXImageResource alloc] initWithImageName:(_name_) bundle:(_bundle_)]

#define CTX_R_IMG_NO_IMAGE CTX_R_IMG(nil, nil)

@interface CTXImageResource : CTXBundleResource

- (instancetype) initWithImageName:(NSString*) imageName
                            bundle: (NSBundle*) bundle;

@end
