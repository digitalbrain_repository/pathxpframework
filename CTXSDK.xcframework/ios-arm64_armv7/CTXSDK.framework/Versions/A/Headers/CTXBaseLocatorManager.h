//
//  CTXBeaconsManagerPlus.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <CTXSDK/CTXBaseTriggerManager.h>

@interface CTXBaseLocatorManager : CTXBaseTriggerManager <CLLocationManagerDelegate>

#pragma mark - Properties
@property (nonatomic, readonly) BOOL isRangingAvailable;
@property (nonatomic, readonly) BOOL locationServicesEnabled;
@property (nonatomic, readonly) CLAuthorizationStatus authorizationStatus;
@property (nonatomic, readonly) BOOL isMonitoringAvailable;
@property (nonatomic, readonly) BOOL significantLocationChangeMonitoringAvailable;
@property (nonatomic, readonly) CLLocationManager *locationManager;

#pragma mark - Functions
- (void)initLocationManager;
- (Class)regionClassToMonitorize;

- (void)startMonitoring;
- (void)stopMonitoring;

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations;

@end
