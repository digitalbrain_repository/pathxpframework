//
//  AIRequestsManager.h
//  FlightApp
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>

// *** REQUESTS ***
#define BASE_URL_REQUEST                @"BASE_URL_REQUEST"
#define PUBLIC_URL_REQUEST              @"PUBLIC_URL_REQUEST"
#define PRIVATE_URL_REQUEST             @"PRIVATE_URL_REQUEST"


@interface CTXURIManager : NSObject

// *** PROPERTIES ***
+ (instancetype)manager;
+ (instancetype)sharedManager;

// *** FUNCTIONS ***
- (NSString*)getBaseUrl;
- (NSString*)getUrlByKey:(NSString*)key;

// *** ADVANCED ***
- (void) overrideBaseUrl: (NSString*) overrideURL;

@end
