//
//  CLConnectionManager.h
//  CoreLibrary
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <Foundation/Foundation.h>

#import <CTXSDK/AFNetworkReachabilityManager.h>

@interface CLConnectionManager : NSObject

@property (nonatomic, assign) BOOL forceOffline;
@property (nonatomic) NSDate * dumbReachabilityTime;
@property (nonatomic, assign) AFNetworkReachabilityStatus currentReachability;



+ (instancetype)manager;
+ (instancetype)sharedManager;

- (void) isOnline: (void(^)(BOOL online)) handler;

- (void) addReachabilityStatusChangeBlock: (void(^)(AFNetworkReachabilityStatus status)) handler withName: (NSString*) name;
- (void) removeReachabilityStatusChangeBlockWithName: (NSString*) name;


@end
