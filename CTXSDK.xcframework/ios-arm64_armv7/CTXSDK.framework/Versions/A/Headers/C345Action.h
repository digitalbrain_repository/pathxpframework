//
//  C345Action.h
//  PathXP
//
// CTXSDK_HEAD_CREATED_BY
// CTXSDK_HEAD_COPYRIGHT
//

#import <CTXSDK/CLJastor.h>

@interface C345Action : CLJastor

@property (nonatomic, copy) NSNumber *actionId;
@property (nonatomic, copy) NSString *actionType;
@property (nonatomic, copy) NSString *layout;

@end
