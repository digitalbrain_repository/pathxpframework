//
//  CTXCacheLinkActionsResponse.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <CTXSDK/C345Response.h>
#import <CTXSDK/CTXCacheBaseActionsResponseBody.h>

#pragma mark - Class CTXCacheLinkActionsResponseBody
@interface CTXCacheLinkActionsResponseBody : CTXCacheBaseActionsResponseBody

#pragma mark - Properties
@property (nonatomic, strong) NSArray *linkActions;

@end

#pragma mark - Class CTXCacheLinkActionsResponse
@interface CTXCacheLinkActionsResponse : C345Response

#pragma mark - Properties
@property (nonatomic) CTXCacheLinkActionsResponseBody *body;

@end

