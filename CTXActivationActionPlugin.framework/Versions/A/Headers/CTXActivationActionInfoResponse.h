//
//  CTXActivationActionInfoResponse.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//
#import <UIKit/UIKit.h>
#import <CTXSDK/C345Response.h>
#import <CTXSDK/CTXBaseActionInfoResponseBody.h>

#pragma mark - Class CTXActivationActionInfoResponse
@interface CTXActivationActionInfoResponseBody : CTXBaseActionInfoResponseBody

#pragma mark - Properties
@property (nonatomic) NSString *text;
@property (nonatomic) NSNumber *imageId;
@property (nonatomic) NSNumber *logoImageId;

#pragma mark - Custom Properties
//@property (nonatomic) UIImage *image;
//@property (nonatomic) UIImage *logoImage;

@end

#pragma mark - Class CTXActivationActionInfoResponse
@interface CTXActivationActionInfoResponse : C345Response

#pragma mark - Properties
@property (nonatomic) CTXActivationActionInfoResponseBody *body;

@end
