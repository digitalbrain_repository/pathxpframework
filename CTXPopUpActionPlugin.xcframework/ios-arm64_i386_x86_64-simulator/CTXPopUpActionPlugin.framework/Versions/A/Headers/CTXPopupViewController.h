//
//  CTXPopupViewController.h
//  ContextSDK
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <UIKit/UIKit.h>
#import <CTXPopupActionPlugin/CTXPopupActionInfoResponse.h>
#import <CTXSDK/CTXBaseActionViewController.h>

@interface CTXPopupViewController : CTXBaseActionViewController

@property (nonatomic) CTXPopupActionInfoResponseBody *popupInfo;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelBody;

@end
