//
//  CTXPopupPushInfoResponse.h
//  ContextSDK
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <CTXSDK/C345Response.h>
#import <CTXSDK/CTXBaseActionInfoResponseBody.h>

#pragma mark - Class CTXPopupActionInfoResponseBody
@interface CTXPopupActionInfoResponseBody : CTXBaseActionInfoResponseBody

#pragma mark - Properties
@property (nonatomic) NSString *popUpTemplateName;
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *body;

@end

#pragma mark - Class CTXPopupActionInfoResponse
@interface CTXPopupActionInfoResponse : C345Response

#pragma mark - Properties
@property (nonatomic) CTXPopupActionInfoResponseBody *body;

@end
