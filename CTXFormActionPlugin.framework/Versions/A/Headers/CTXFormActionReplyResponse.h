//
//  CTXFormActionReplyResponse.h
//  FormActionPlugin
//
//  Created by Massimiliano Schinco on 18/01/17.
//  Copyright © 2017 Massimiliano Schinco. All rights reserved.
//

#import <CTXSDK/C345Response.h>
#import <CTXSDK/CTXCallToAction.h>
@interface CTXFormActionReplyResponseBody: CTXResponseBody
@property (nonatomic) NSNumber *interactionId;
@property (nonatomic) NSString *type;
@end

@interface CTXFormActionReplyResponse : C345Response
@property (nonatomic) CTXFormActionReplyResponseBody *body;
@end
