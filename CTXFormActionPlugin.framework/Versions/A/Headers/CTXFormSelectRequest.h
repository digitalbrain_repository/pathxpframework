//
//  CTXFormSelectRequest.h
//  FormActionPlugin
//
//  Created by Massimiliano on 06/09/17.
//  Copyright © 2017 Massimiliano Schinco. All rights reserved.
//

#import <CTXSDK/C345Request.h>

#import <CTXFormActionPlugin/CTXFormActionInfoResponse.h>

@interface CTXFormSelectRequestBody: CTXRequestBody
/*@property (nonatomic) CTXFormKeyValue* current;
@property (nonatomic) CTXFormKeyValue* parent;*/
@end


@interface CTXFormSelectRequest : C345Request <CTXRequestDelegate>
- (instancetype)initWithActionName:(NSString *)actionName;
@property (nonatomic) CTXFormSelectRequestBody *body;
@end


