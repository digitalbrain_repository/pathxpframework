//
//  CTXFormActionReplyRequest.h
//  FormActionPlugin
//
//  Created by Massimiliano Schinco on 17/01/17.
//  Copyright © 2017 Massimiliano Schinco. All rights reserved.
//

#import <CTXSDK/C345Request.h>
@interface CTXFormActionReplyItem: CLJastor
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *type;
@property (nonatomic) NSString *value;
@end

@interface CTXFormActionReplyRequestBody: CTXRequestBody
@property (nonatomic) NSArray *items;
@property (nonatomic) NSNumber *userFeedbackId;
@property (nonatomic) NSNumber *formActionId;
@end

@interface CTXFormActionReplyRequest : C345Request <CTXRequestDelegate>
@property (nonatomic) CTXFormActionReplyRequestBody *body;
- (instancetype)initWithTraceID:(NSString*)traceID;
@end
