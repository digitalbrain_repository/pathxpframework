//
//  CTXFormActionInfoResponse.h
//  FormActionPlugin
//
//  Created by Massimiliano Schinco on 16/01/17.
//  Copyright © 2017 Massimiliano Schinco. All rights reserved.
//

#import <CTXSDK/CTXBaseActionInfoResponseBody.h>
#import <CTXFormActionPlugin/CTXFormActionReplyRequest.h>



@interface CTXFormRadio : CLJastor
@property (nonatomic) NSNumber *order;
@property (nonatomic) NSString *label;
@property (nonatomic) NSString *name;
@property (nonatomic) NSNumber *checked;
@end

@interface CTXFormNote : CLJastor
@property (nonatomic) NSNumber *order;
@property (nonatomic) NSString *mandatory;
@property (nonatomic) NSString *position;
@property (nonatomic) NSString *value;
@end

@interface CTXFormSelectItem : CLJastor
@property (nonatomic) NSNumber *order;
@property (nonatomic) NSString *label;
@property (nonatomic) NSString *value;
@end

@interface CTXFormRange : CLJastor
@property (nonatomic) NSNumber *min;
@property (nonatomic) NSNumber *max;

@end

@interface CTXFormItem : CLJastor

@property (nonatomic) NSNumber *order;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *type;
@property (nonatomic) NSString *label;
@property (nonatomic) NSString *defaultValue;
@property (nonatomic) NSString *placeholder;
@property (nonatomic) CTXFormRange *range;
@property (nonatomic) NSNumber *mandatory;
@property (nonatomic) NSString *dateFormat;
@property (nonatomic) NSArray *select;
@property (nonatomic) NSArray *radios;
@property (nonatomic) NSString *radiogroup;
@property (nonatomic) NSString *value;
@property (nonatomic) NSString *descr;
@property (nonatomic) NSNumber *showPrintableLabel;
@property (nonatomic) NSString *printableLabel;

- (CTXFormActionReplyItem *)convertToReplyItem;


@end

@interface CTXFormActionInfoResponseBody : CTXBaseActionInfoResponseBody
@property (nonatomic) NSArray *form;
@property (nonatomic) NSArray *formNote;
@property (nonatomic) NSString *title;
@property (nonatomic) NSNumber *userFeedbackId;
@property (nonatomic) NSNumber *formActionId;
@property (nonatomic) NSNumber *dataRecap;
@end

@interface CTXFormActionInfoResponse : C345Response
@property (nonatomic) CTXFormActionInfoResponseBody *body;
@end



