//
//  CTXSectionPushInfoResponse.h
//  ContextSDK
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//
#import <UIKit/UIKit.h>
#import <CTXSDK/C345Response.h>
#import <CTXSDK/CTXBaseActionInfoResponseBody.h>

@interface CTXSectionActionBeaconAppParams : CLJastor

@property (nonatomic, copy) NSNumber *beaconTriggerId;
@property (nonatomic, copy) NSString *beaconMacAddress;
@property (nonatomic, copy) NSString *appParams;

@end

#pragma mark - Class CTXSectionActionInfoResponseBody
@interface CTXSectionActionInfoResponseBody : CTXBaseActionInfoResponseBody

#pragma mark - Properties
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *text;
@property (nonatomic) NSString *sectionName;
@property (nonatomic) NSString *appParams;
@property (nonatomic) NSNumber *imageId;
@property (nonatomic, strong) NSArray *beaconAppParamsList;

#pragma mark - Custom Properties
//@property (nonatomic) UIImage *image;

@property (nonatomic, readonly) NSDictionary * appParamsDictionary;

@end

#pragma mark - Class CTXSectionActionInfoResponse
@interface CTXSectionActionInfoResponse : C345Response

#pragma mark - Properties
@property (nonatomic) CTXSectionActionInfoResponseBody *body;

@end
