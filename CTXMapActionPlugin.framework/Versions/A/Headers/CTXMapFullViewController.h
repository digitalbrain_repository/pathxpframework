//
//  CTXMapFullViewController.h
//  PathXP
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CTXMapActionPlugin/CTXMapActionInfoResponse.h>
#import <CTXSDK/CTXBaseActionViewController.h>

#define CTX_MAP_FULL_VIEWCONTROLLER_IDENTIFIER   @"CTXMapFullVCIdentifier"

@interface CTXMapFullViewController : CTXBaseActionViewController <MKMapViewDelegate>

#pragma mark - Outlets
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIButton *buttonChiudi;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewLogo;
@property (weak, nonatomic) IBOutlet UIImageView *headerImageView;
@property (weak, nonatomic) IBOutlet UILabel *labelText;
@property (weak, nonatomic) IBOutlet UILabel *labelInfoWindow;


#pragma mark - Properties
@property (nonatomic) CTXMapActionInfoResponseBody *mapInfo;

#pragma mark - Actions
- (IBAction)buttonChiudiAction:(id)sender;
- (IBAction)buttonGetDirectionsAction:(id)sender;

@end
