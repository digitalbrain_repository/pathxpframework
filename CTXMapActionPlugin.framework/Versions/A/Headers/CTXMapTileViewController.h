//
//  CTXMapTileViewController.h
//  ContextSDK
//
//  Made with ❤ by The Context Platform Team
//  Copyright (c) 2016 The Context Platform - build: 8b807ed9015b020ba8f6c1fabd76d9a1ae38396f
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CTXMapActionPlugin/CTXMapActionInfoResponse.h>
#import <CTXSDK/CTXGenericTileViewController.h>

#define CTX_MAP_TILE_VIEWCONTROLLER_IDENTIFIER   @"CTXMapTileVCIdentifier"

@interface CTXMapTileViewController : CTXGenericTileViewController <MKMapViewDelegate>

#pragma mark - Outlets
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end
